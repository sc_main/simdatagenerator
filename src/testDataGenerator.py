from DataGenerator import *

import os
import json
try:
    with open(os.getcwd() + "\\input\\sampleSimData.json") as f:
        simData = json.load(f)
except:
    print("sampleSimData.json file cannot be found.")

generateDeliverySimsFromSimData(simData)