from SimDataGenInterface import *
import sys
from PyQt5.QtWidgets import QApplication

if __name__ == '__main__':
    print("HX Delivery Simulator")
    app = QApplication(sys.argv)
    dialog = SimDataGenInterface()
    sys.exit(dialog.exec_())
