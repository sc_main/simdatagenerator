* Programın çalışması için districtList.json dosyası input klasörü içinde olmalı. Mahalle 
bilgileri bu dosyadan okunuyor.

* Simulasyon parametreleri excel dosyasından okunuyor. Bu dosyanın özellikleri scenario_template
 dosyasında görülebilir. Dosyada percents, coefficients ve deliveries sekmeleri olmalıdır. 
 Sekmelerdeki bilgilerin özellikleri şunlardır:
	Percents : Std (Standart), ex (express delivery), bt (bugun teslimat), yt (yarin teslimat) 
	tipindeki gonderilerin gun icindeki yogunluklari girilir. bt1,bt2,bt3,yt1,yt2,yt3 birinci 
	, ikinci, ucuncu slot icin gelen teslimatlari belirtir. Burada girilen degerler sadece ORAN
	belirtir. Toplamlarinin bir etmesine gerek yoktur. Toplamlari birden buyuk de olsa, toplam 
	teslimat yogunlugunun o saate oranini belirtir.
	Coefficients : Bu sekmede "ex coef bt" alanina bir deger girilirse gunluk bt ve yt sayisinin 
	buradaki deger kadar kati sayida gunluk express teslimat gelmesi beklenir. Buradaki deger sifir 
	ise ve "ex coef" alaninda 100 gibi bir deger varsa, bu durumda bt, yt sayisindan etkilenmeden gunde
	100 express teslimat gelmesi beklenir. exSlot0, exSlot1, exSlot2, exSlot3 alanlari express teslimatlarin
	oranlarini belirler. exSlot0 , bir saat icinde teslimati gosterir. Digerleri de secilebilen saatlik 
	araliklarini sirayla gosterir.
	deliveries : Bu sekmede mahallelerde gunluk gelen std, bt1, bt2, bt3, yt1, yt2, yt3 tipindeki teslimatlarin
	SAYILARI girilir. Buradaki degerler oran degil direk olarak beklenen teslimat sayisidir. 
	
deliveries sekmesinde girilen degerleri kullanarak ancak herseyin belli bir oranda kati kadar
teslimat gelmesini saglamak icin urunun arayuzundeki MASTER katsayi kullanilir. Ornegin buraya 2 
degeri girilirse beklenen senaryonun iki kati kadar teslimat olusmasi saglanir.
Mon, Tue.... alanlarina girilen degerler de simulasyonun calistigi gunun hangi gune denk geldigine
gore degisen bir katsayidir. Ornegin simulasyon carsamba gunu icin calistiginda (20.03.2019) Wed
alanındaki katsayı kullanılır. Simulasyon 10 günlük gibi bir aralıkta çalıştığında bu alanlar faydalı
olacaktır.
	