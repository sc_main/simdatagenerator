
def addNodeToPathNodeIdPlace(path, nodeId, place):
    tempPath = path.copy()
    tempPath.insert(place, nodeId)
    return tempPath

def addNodeToPathNodeId(path, nodeId):
    for i in range(len(path)):
        tempPath = addNodeToPathNodeIdPlace(path, nodeId, i)

path = []

path = addNodeToPathNodeIdPlace(path, 10, 0)
print(path)

path = addNodeToPathNodeIdPlace(path, 3, 1)
print(path)

path = addNodeToPathNodeIdPlace(path, 7, 1)
print(path)

