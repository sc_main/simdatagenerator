import pandas as pd
import os
import json
from DistrictSplitTest import *

simData = {}
try:
    with open(os.getcwd() + "\\input\\districtList.json") as f:
        districtListJson = json.load(f)
except:
    print("districtList.json file cannot be found.")
simData["districtList"] = districtListJson["districtList"]

try:
    with open(os.getcwd() + "\\input\\districtDistances.json") as f:
        districtDistancesJson = json.load(f)
except:
    print("districtList.json file cannot be found.")
simData["districtDistances"] = districtDistancesJson["distanceMatrix"]

simDataXls = pd.ExcelFile(os.getcwd() + "\\input\\scenario_01.xlsx")
testDistrictSplit(simDataXls, simData)

