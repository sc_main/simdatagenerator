import datetime
import os

def getDateStrMinimal(time):
    timeStr = time.strftime("%Y%m%d_%H%M%S")
    return timeStr

def getNowStrMinimal():
    time = datetime.datetime.now()
    return getDateStrMinimal(time)

def createFolderWithCheck(dir):
    if (not os.path.exists(dir)):
        os.mkdir(dir)

def writeJsonToFileLocal(data, fileName):
    cwd = os.getcwd()
    destDir = cwd + "\\output\\"
    createFolderWithCheck(destDir)
    nowStr = getNowStrMinimal()
    destDir += nowStr + "_"
    textFile = open(destDir + fileName + "." + "json"  , "w")
    textFile.write(data)
    textFile.close()