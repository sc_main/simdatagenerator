from PoissonDist import *
from FileMethods import *
from RouletteSelection import *
from DateMethods import *
from ExcelWriter import *
from SimDataGenConstants import *
import datetime
import json

def generateDeliverySimsFromSimData(simData, districtIdGiven = 0):
    success = True
    try :
        deliverySimsList = generateDeliverySimsFromScenario(simData, districtIdGiven)
        deliverySimsJson = getDeliverySimsJson(deliverySimsList)
        writeDeliverySimsToJson(deliverySimsJson)
        writeDeliverySimsExcelFile(deliverySimsList)
    except :
        success = False
    return success

def writeDeliverySimsToJson(deliverySimsJson):
    try:
        writeJsonToFileLocal(json.dumps(deliverySimsJson), "deliverySims")
    except:
        print("Exception at writeDeliverySimsToJson")

def generateDeliverySimsFromScenario(simData, districtIdGiven=0):
    deliverySimsAll = []
    simBegDate = createDateFromDateJson(simData.get("begDate"))
    simEndDate = createDateFromDateJson(simData.get("endDate"))

    coefs = simData.get("coefs")
    masterCoef = coefs.get("masterCoef")
    dayCoefs = coefs.get("dayCoefs")

    districtScenarios = simData["districtScenarios"]
    deliveryTypes = simData["info"]["deliveryTypes"]
    simDate = copyDate(simBegDate)
    while dateToStr(simDate) <= dateToStr(simEndDate):
        weekDay = simDate.weekday()
        dayCoef = dayCoefs[weekDay]
        for districtId in districtScenarios.keys():
            if (int(districtId) == ALL_DISTRICTS):
                continue
            districtName, geoloc_lat, geoloc_lng = getDistrictInfo(simData["districtList"], districtId)
            if districtIdGiven == 0 or districtIdGiven == int(districtId):
                scenarioForDistrict = districtScenarios[districtId]
                for deliveryType in range(len(deliveryTypes)):
                    deliveryTypeStr = deliveryTypes[deliveryType]
                    percents = {}
                    if (deliveryTypeStr == "bt"):
                        percents = scenarioForDistrict["btPercents"]
                    elif (deliveryTypeStr == "yt"):
                        percents = scenarioForDistrict["ytPercents"]
                    elif (deliveryTypeStr == "ex"):
                        percents = scenarioForDistrict["exPercents"]
                    scenarioHours = scenarioForDistrict.get(deliveryTypeStr)
                    for hourInterval in range(len(scenarioHours)):
                        scenarioHour = scenarioHours[hourInterval]
                        scenarioHourWithCoef = scenarioHour * dayCoef * masterCoef
                        if scenarioHourWithCoef > 0:
                            times = generateTimesForOneHour(scenarioHourWithCoef)
                            deliverySims = processTimes(districtName, geoloc_lat, geoloc_lng, times,
                                                        hourInterval, deliveryType, deliveryTypeStr,
                                                        simDate, districtId, percents)
                            deliverySimsAll.extend(deliverySims)
        simDate = simDate + datetime.timedelta(days=1)
    deliverySimsAllSorted = sorted(deliverySimsAll, key=lambda k: k['deliveryDate'])
    deliveryId = 1
    for deliverySim in deliverySimsAllSorted:
        deliverySim["id"] = deliveryId
        deliveryId += 1
    return deliverySimsAllSorted

def processTimes(districtName, geoloc_lat, geoloc_lng, times,
                 hourInterval, deliveryType, deliveryTypeStr,
                 simDate, districtId, percents):
    deliverySims = []
    for timeSeconds in times:
        deliverySim = {}
        hour = int(timeSeconds / 3600)
        timeSeconds = timeSeconds - hour * 3600
        minute = int(timeSeconds / 60)
        second = timeSeconds - minute * 60
        hour = hour + hourInterval
        deliveryDate = simDate.replace(hour = hour, minute = minute, second= second)
        deliverySim["deliveryDate"] = dateToStr(deliveryDate)
        deliverySim["deliveryType"] = deliveryType
        deliverySim["deliveryTypeName"] = getDeliveryTypeName(deliveryTypeStr)
        deliverySim["districtId"] = districtId
        deliverySim["districtName"] = districtName
        slot = generateDeliverySlot(deliveryTypeStr, hourInterval, percents, deliveryDate)
        deliverySim["slot"] = slot
        productType = generateProductType(deliveryTypeStr)
        deliverySim["productType"] = productType
        deliverySim["productTypeName"] = getProductTypeName(productType)
        deliverySim["geoloc_lat"] = geoloc_lat - DISTRICT_RADIUS + random.random()*DISTRICT_RADIUS*2
        deliverySim["geoloc_lng"] = geoloc_lng - DISTRICT_RADIUS + random.random()*DISTRICT_RADIUS*2
        promisedBegDate, promisedEndDate, prepationDate = calcPromisedDates(deliveryTypeStr, slot, deliveryDate)
        deliverySim["slotName"] = getSlotName(promisedBegDate, promisedEndDate, deliveryTypeStr, slot)
        deliverySim["promisedBegDate"] = dateToStr(promisedBegDate)
        deliverySim["promisedEndDate"] = dateToStr(promisedEndDate)
        deliverySim["prepationDate"] = dateToStr(prepationDate)
        deliverySims.append(deliverySim)
    return deliverySims

def calcPromisedDates(deliveryTypeStr, slot, deliveryDate):
    deliveryDateHour = deliveryDate.hour
    promisedBegDate = copyDate(deliveryDate)
    promisedEndDate = copyDate(deliveryDate)
    prepationDate = copyDate(deliveryDate)

    if (deliveryTypeStr == "std" or deliveryTypeStr == "bt" or deliveryTypeStr == "yt" or (deliveryTypeStr == "ex" and slot != -1)):
        promisedBegDate = promisedBegDate.replace(minute=0, second=0)
        promisedEndDate = promisedEndDate.replace(minute=0, second=0)
    if deliveryTypeStr == "std":
        if deliveryDateHour < 22:
            promisedBegDate = promisedBegDate + datetime.timedelta(days=1)
        else:
            promisedBegDate = promisedBegDate + datetime.timedelta(days=2)
        promisedBegDate = promisedBegDate.replace(hour=9)
        promisedEndDate = promisedBegDate + datetime.timedelta(days=2)
        promisedEndDate = promisedEndDate.replace(hour=18)
        prepationDate = promisedEndDate.replace(hour=11, minute=45)
    elif deliveryTypeStr == "bt":
        if slot == 1:
            promisedBegDate = promisedBegDate.replace(hour=9)
            promisedEndDate = promisedEndDate.replace(hour=13)
        elif slot == 2:
            promisedBegDate = promisedBegDate.replace(hour=13)
            promisedEndDate = promisedEndDate.replace(hour=18)
        elif slot == 3:
            promisedBegDate = promisedBegDate.replace(hour=18)
            promisedEndDate = promisedEndDate.replace(hour=23)
    elif deliveryTypeStr == "yt":
        promisedBegDate = promisedBegDate + datetime.timedelta(days=1)
        promisedEndDate = promisedEndDate + datetime.timedelta(days=1)
        if slot == 1:
            promisedBegDate = promisedBegDate.replace(hour=9)
            promisedEndDate = promisedEndDate.replace(hour=13)
        elif slot == 2:
            promisedBegDate = promisedBegDate.replace(hour=13)
            promisedEndDate = promisedEndDate.replace(hour=18)
        elif slot == 3:
            promisedBegDate = promisedBegDate.replace(hour=18)
            promisedEndDate = promisedEndDate.replace(hour=23)
    elif deliveryTypeStr == "ex":
        if slot == -1:
            promisedEndDate = promisedEndDate + datetime.timedelta(hours=1)
        else :
            promisedBegDate = promisedBegDate.replace(hour=slot)
            promisedEndDate = promisedBegDate + datetime.timedelta(hours= 1)
    if (deliveryTypeStr == "bt" or deliveryTypeStr == "yt" or deliveryTypeStr == "ex"):
        minuteDiff = -1 * (10 + int(random.random()*10))
        prepationDate = promisedBegDate + datetime.timedelta(minutes=minuteDiff)
        prepationDate = prepationDate.replace(second=0)

    return promisedBegDate, promisedEndDate, prepationDate

def generateProductType(deliveryTypeStr):
    if deliveryTypeStr == "ex":
        weights = [50, 10, 10, 10, 10, 10]
    else :
        weights = [60, 10, 10, 10, 10]
    productType = rouletteChoose(weights)
    return productType

def getProductTypeName(productType):
    switcher = {
        0: "Genel",
        1: "Market Urunu",
        2: "Temizlik Urunu",
        3: "Elektronik",
        4: "Kitap",
        5: "Soguk zincir",
    }
    return switcher.get(productType, "Invalid Product Type")

def getSlotName(promisedBegDate, promisedEndDate, deliveryTypeStr, slot):
    if deliveryTypeStr == "std":
        return "-"
    elif (deliveryTypeStr == "bt" or deliveryTypeStr == "yt"):
        return str(slot) + ". slot";
    elif (deliveryTypeStr == "ex"):
        if slot == -1:
            return "* " + dateToStrOnlyHour(promisedBegDate)+"-"+dateToStrOnlyHour(promisedEndDate)
        else:
            return dateToStrOnlyHour(promisedBegDate) + "-" + dateToStrOnlyHour(promisedEndDate)
    return "Invalid slot name"

def generateDeliverySlot(deliveryTypeStr, hourInterval, percents, deliveryDate):
    slot = 0
    if (deliveryTypeStr == "bt"):
        if hourInterval < 4:
            weights = [percents[0], percents[1], percents[2]]
            selection = rouletteChoose(weights)
            slot = selection + 1
        elif hourInterval < 9:
            weights = [percents[4], percents[5]]
            selection = rouletteChoose(weights)
            slot = selection + 2
        else:
            slot = 3
    elif (deliveryTypeStr == "yt"):
        if hourInterval < 8:
            weights = [percents[0], percents[1], percents[2]]
            selection = rouletteChoose(weights)
            slot = selection + 1
        elif hourInterval < 16:
            weights = [percents[3], percents[4], percents[5]]
            selection = rouletteChoose(weights)
            slot = selection + 1
        else:
            weights = [percents[6], percents[7], percents[8]]
            selection = rouletteChoose(weights)
            slot = selection + 1
    elif (deliveryTypeStr == "ex"):
        weights = [percents[0], percents[1], percents[2], percents[3]]
        selection = rouletteChoose(weights)
        slotSelection = 0
        if isW1hOpen(hourInterval):
            slotSelection = selection
        else:
            if selection == 0:
                slotSelection = 1
            else:
                slotSelection = selection
        if slotSelection == 0:
            slot = -1
        else:
            slot = findFirstExSlot(deliveryDate, slotSelection)
    return slot

def isW1hOpen(hour):
    return hour >= 8 and hour <= 22

def findFirstExSlot(deliveryDate, slotSelection):
    tempDate = deliveryDate + datetime.timedelta(minutes=90)
    tempHour = tempDate.hour
    slotHour = tempHour + slotSelection
    slotHourMod = slotHour % 24
    if slotHourMod < 8:
        return 8
    return slotHourMod

def getDeliveryTypeName(deliveryTypeStr):
    switcher = {
        "std": "Standart Teslimat",
        "bt": "Bugun Teslimat",
        "yt": "Yarin Teslimat",
        "ex": "Ekspres Teslimat"
    }
    return switcher.get(deliveryTypeStr, "Invalid Delivery Type")

def getDeliverySimsJson(deliverySims):
    deliverySimsJson = {}
    deliverySimsJson["deliverySims"] = deliverySims
    return deliverySimsJson

def getDistrictInfo(districtList, districtId):
    for districtItem in districtList:
        districtIdInList = districtItem["id"]
        if districtId == str(districtIdInList):
            return districtItem.get("shortName"), districtItem.get("lat"), districtItem.get("lng")
    return "Invalid district name", 0, 0
