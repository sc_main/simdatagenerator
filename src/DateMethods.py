import datetime
import time
from time import mktime

DATE_FORMAT = "%d.%m.%Y %H:%M:%S"

def dateToStr(date):
    if date != None:
        return date.strftime(DATE_FORMAT)
    return "-"

def strToDate(dateStr):
    dDate = time.strptime(dateStr, DATE_FORMAT)
    return datetime.fromtimestamp(mktime(dDate))

def dateToStrOnlyHour(date):
    if date != None:
        return date.strftime("%H:%M")
    return "-"

def copyDate(date):
    newDate = datetime.datetime.now()
    newDate = newDate.replace(year = date.year, month = date.month, day = date.day,
                    hour = date.hour, minute = date.minute, second = date.second)
    return newDate

def createDateJsonFromDate(date):
    dateJson = {}
    dateJson["day"] =  date.day()
    dateJson["month"] = date.month()
    dateJson["year"] = date.year()
    return dateJson

def createDateFromDateJson(dateJson):
    year = dateJson.get("year")
    month = dateJson.get("month")
    day = dateJson.get("day")
    return datetime.datetime(year, month, day)

def secondsToHoursStr(seconds):
    hours = int(seconds / 3600)
    seconds = seconds % 3600
    minutes = int(seconds / 60)
    seconds = seconds % 60
    return str(hours) + "h " + str(minutes) + "m " + str(seconds) + "s "