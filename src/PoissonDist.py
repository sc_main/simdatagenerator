import math
import random

def findNextTime(rateParameter):
    return -math.log(1.0 - random.random()) / rateParameter


def generateTimes(numExpected, totalTime):
    times = []
    if numExpected > 0:
        rateParameter = 1.0 / (totalTime / numExpected)
        currentTime = 0
        finished = False
        while not finished:
            nextTime = int(findNextTime(rateParameter))
            currentTime += nextTime
            if currentTime >= totalTime:
                finished = True
            if not finished:
                times.append(currentTime)
    return times

def generateTimesForOneHour(numExpected):
    return generateTimes(numExpected, 3600)