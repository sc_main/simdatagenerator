from operator import add

import pandas as pd
from PyQt5 import QtGui, QtCore
from PyQt5.QtWidgets import (QComboBox, QDialog, QLabel, QVBoxLayout,
                             QPushButton, QDoubleSpinBox, QAbstractSpinBox,
                             QHBoxLayout, QDateEdit, QMessageBox, QFileDialog)

from ArrayMethods import *
from DataGenerator import *
from DistrictSplitTest import *


class SimDataGenInterface(QDialog):
    def __init__(self):
        super(SimDataGenInterface, self).__init__()
        self.debugCounter = 0

        self.simData = {}
        self.simData["info"] = {}
        self.simData["info"]["deliveryTypes"] = []
        self.simData["info"]["deliveryTypes"].append("std")
        self.simData["info"]["deliveryTypes"].append("bt")
        self.simData["info"]["deliveryTypes"].append("yt")
        self.simData["info"]["deliveryTypes"].append("ex")
        self.simData["districtScenarios"] = {}
        try:
            print(os.getcwd() + "\\input\\districtList.json")
            with open(os.getcwd() + "\\input\\districtList.json") as f:
                districtListJson = json.load(f)
        except:
            print("districtList.json file cannot be found.")
        self.simData["districtList"] = districtListJson["districtList"]

        try:
            with open(os.getcwd() + "\\input\\districtDistances.json") as f:
                districtDistancesJson = json.load(f)
        except:
            print("districtList.json file cannot be found.")
        self.simData["districtDistances"] = districtDistancesJson["distanceMatrix"]

        self.setGeometry(400, 300, 800, 400)
        self.activeDistrictId = ALL_DISTRICTS

        self.font_1 = QtGui.QFont("Cambria", 9, QtGui.QFont.Normal)
        self.font_2 = QtGui.QFont("Cambria", 12, QtGui.QFont.Normal)
        self.font_3 = QtGui.QFont("Cambria", 16, QtGui.QFont.Normal)

        self.mainLayout = QVBoxLayout()

        self.emptyLayout = QVBoxLayout()
        self.emptyLabel = self.createLabelCenteredWithSize("", self.emptyLayout, 100)

        self.areaSelectionLayout = QHBoxLayout()

        self.cb1 = QComboBox()
        self.cb1.setMinimumHeight(30)
        self.cb1.setFont(self.font_2)
        self.areaSelectionLayout.addWidget(self.cb1)

        self.cb2 = QComboBox()
        self.cb2.setMinimumHeight(30)
        self.cb2.setFont(self.font_2)
        self.areaSelectionLayout.addWidget(self.cb2)

        self.cb3 = QComboBox()
        self.cb3.setMinimumHeight(30)
        self.cb3.setFont(self.font_2)
        self.areaSelectionLayout.addWidget(self.cb3)

        self.cb4 = QComboBox()
        self.cb4.setMinimumHeight(30)
        self.cb4.setFont(self.font_2)
        self.areaSelectionLayout.addWidget(self.cb4)

        self.screenButtonsLayout = QHBoxLayout()

        self.buttonLoadExcel = QPushButton("Load From Excel")
        self.buttonLoadExcel.setMinimumHeight(30)
        self.buttonLoadExcel.setFont(self.font_2)
        self.buttonLoadExcel.clicked.connect(self.loadFromExcel)
        self.screenButtonsLayout.addWidget(self.buttonLoadExcel)

        self.buttonSaveExcel = QPushButton("Save To Excel")
        self.buttonSaveExcel.setMinimumHeight(30)
        self.buttonSaveExcel.setFont(self.font_2)
        self.buttonSaveExcel.clicked.connect(self.saveToExcel)
        self.screenButtonsLayout.addWidget(self.buttonSaveExcel)

        self.buttonLoadJson = QPushButton("Load From Json")
        self.buttonLoadJson.setMinimumHeight(30)
        self.buttonLoadJson.setFont(self.font_2)
        self.buttonLoadJson.clicked.connect(self.loadFromJson)
        self.screenButtonsLayout.addWidget(self.buttonLoadJson)

        self.buttonS = QPushButton("Save To Json")
        self.buttonS.setMinimumHeight(30)
        self.buttonS.setFont(self.font_2)
        self.buttonS.clicked.connect(self.saveButton)
        self.screenButtonsLayout.addWidget(self.buttonS)

        self.buttonRa = QPushButton("Reset All")
        self.buttonRa.setMinimumHeight(30)
        self.buttonRa.setFont(self.font_2)
        self.buttonRa.clicked.connect(self.resetAllButton)
        self.screenButtonsLayout.addWidget(self.buttonRa)

        self.buttonR = QPushButton("Reset District")
        self.buttonR.setMinimumHeight(30)
        self.buttonR.setFont(self.font_2)
        self.buttonR.clicked.connect(self.resetDistrictButton)
        self.screenButtonsLayout.addWidget(self.buttonR)

        self.buttonCreateEx = QPushButton("Create Ex")
        self.buttonCreateEx.setMinimumHeight(30)
        self.buttonCreateEx.setFont(self.font_2)
        self.buttonCreateEx.clicked.connect(self.createExDeliveriesButton)
        # self.screenButtonsLayout.addWidget(self.buttonCreateEx)

        self.buttonTestScenario = QPushButton("Test Scenario")
        self.buttonTestScenario.setMinimumHeight(30)
        self.buttonTestScenario.setFont(self.font_2)
        self.buttonTestScenario.clicked.connect(self.testScenarioButton)
        self.screenButtonsLayout.addWidget(self.buttonTestScenario)

        self.coefsLayout = QHBoxLayout()

        self.dayCoefsLayout = QVBoxLayout()
        self.daysLabelsLayout = QHBoxLayout()
        labelSize = 40
        self.createLabelCenteredWithSize("Mon", self.daysLabelsLayout, labelSize)
        self.createLabelCenteredWithSize("Tue", self.daysLabelsLayout, labelSize)
        self.createLabelCenteredWithSize("Wed", self.daysLabelsLayout, labelSize)
        self.createLabelCenteredWithSize("Thu", self.daysLabelsLayout, labelSize)
        self.createLabelCenteredWithSize("Fri", self.daysLabelsLayout, labelSize)
        self.createLabelCenteredWithSize("Sat", self.daysLabelsLayout, labelSize)
        self.createLabelCenteredWithSize("Sun", self.daysLabelsLayout, labelSize)
        self.dayCoefsLayout.addLayout(self.daysLabelsLayout)
        self.daysValsLayout = QHBoxLayout()
        self.dayCoefs = self.createNumEdit(self.daysValsLayout, 7, 40)
        self.dayCoefsLayout.addLayout(self.daysValsLayout)

        self.masterCoefLayout = QVBoxLayout()
        self.masterCoefLabelLayout = QHBoxLayout()
        self.createLabelCenteredWithSize("Master", self.masterCoefLabelLayout, labelSize)
        self.masterCoefValueLayout = QHBoxLayout()
        self.masterCoef = self.createNumEdit(self.masterCoefValueLayout, 1, 40)
        self.masterCoefLayout.addLayout(self.masterCoefLabelLayout)
        self.masterCoefLayout.addLayout(self.masterCoefValueLayout)

        self.emptyLabelSmall = QLabel("")
        self.emptyLabelSmall.setMaximumHeight(15)
        self.emptyLabelSmall.setMaximumWidth(40)

        self.coefsLayout.addLayout(self.masterCoefLayout)
        self.coefsLayout.addWidget(self.emptyLabelSmall)
        self.coefsLayout.addLayout(self.dayCoefsLayout)
        self.coefsLayout.addWidget(self.emptyLabelSmall)

        self.controlButtonsWithDaysLayout = QHBoxLayout()

        self.controlButtonsWithoutDaysLayout = QVBoxLayout()
        self.controlButtonsLayout = QHBoxLayout()

        self.begDate = QDateEdit()
        self.begDate.setMinimumHeight(30)
        self.begDate.setMaximumWidth(150)
        self.begDate.setFont(self.font_2)
        self.begDate.setDisplayFormat('dd-MM-yyyy')
        self.begDate.setAlignment(QtCore.Qt.AlignCenter)
        self.controlButtonsLayout.addWidget(self.begDate)

        self.endDate = QDateEdit()
        self.endDate.setMinimumHeight(30)
        self.endDate.setMaximumWidth(150)
        self.endDate.setFont(self.font_2)
        self.endDate.setDisplayFormat('dd-MM-yyyy')
        self.endDate.setAlignment(QtCore.Qt.AlignCenter)
        self.controlButtonsLayout.addWidget(self.endDate)

        self.buttonGenAll = QPushButton("Generate All")
        self.buttonGenAll.setMinimumHeight(30)
        self.buttonGenAll.setFont(self.font_2)
        self.buttonGenAll.clicked.connect(self.generateAllButton)
        self.controlButtonsLayout.addWidget(self.buttonGenAll)

        self.buttonGenDist = QPushButton("Generate District")
        self.buttonGenDist.setMinimumHeight(30)
        self.buttonGenDist.setFont(self.font_2)
        self.buttonGenDist.clicked.connect(self.generateDistrictButton)
        self.controlButtonsLayout.addWidget(self.buttonGenDist)

        self.buttonC = QPushButton("Close")
        self.buttonC.setMinimumHeight(30)
        self.buttonC.setFont(self.font_2)
        self.buttonC.clicked.connect(self.closeButton)
        self.controlButtonsLayout.addWidget(self.buttonC)

        self.controlButtonsWithoutDaysLayout.addLayout(self.emptyLayout)
        self.controlButtonsWithoutDaysLayout.addLayout(self.controlButtonsLayout)

        self.controlButtonsWithDaysLayout.addLayout(self.controlButtonsWithoutDaysLayout)

        self.cb1.addItem("İstanbul")
        self.cb2.addItem("Kadıköy")

        self.districtNames = []
        self.districtIds = []

        self.cb3.addItem("__Toplam__")

        self.cb4.addItem("Pazartesi")
        self.cb4.addItem("Salı")
        self.cb4.addItem("Çarsamba")
        self.cb4.addItem("Perşembe")
        self.cb4.addItem("Cuma")
        self.cb4.addItem("Cumartesi")
        self.cb4.addItem("Pazar")

        self.numDistricts = 0
        for districtItem in self.simData["districtList"]:
            if (districtItem["area"] != 0):
                self.numDistricts += 1
                districtName = districtItem["shortName"]
                self.districtNames.append(districtName)
                self.districtIds.append(districtItem["id"])
                self.cb3.addItem(districtName + " mahallesi")

        self.cb3.currentIndexChanged.connect(self.districtChange)

        self.simDataLayout = QVBoxLayout()

        self.hoursLayout = QHBoxLayout()

        self.labelLayout = QVBoxLayout()
        self.lbEmpty = self.createLabel("", self.labelLayout)
        self.lb1 = self.createLabel("STD", self.labelLayout)
        self.lb2 = self.createLabel("BT", self.labelLayout)
        self.lb2 = self.createLabel("YT", self.labelLayout)
        self.lb2 = self.createLabel("EX", self.labelLayout)

        self.teLayout = QVBoxLayout()

        self.hours = QHBoxLayout()
        self.createHourLabels()

        self.teLayout.addLayout(self.hours)

        self.layout24_1 = QHBoxLayout()
        self.boxes24_1 = self.createNumEdit(self.layout24_1, 24, 40)
        self.stdTot = self.createNumEdit(self.layout24_1, 1, 50)
        self.teLayout.addLayout(self.layout24_1)

        self.layout24_2 = QHBoxLayout()
        self.boxes24_2 = self.createNumEdit(self.layout24_2, 24, 40)
        self.btTot = self.createNumEdit(self.layout24_2, 1, 50)
        self.teLayout.addLayout(self.layout24_2)

        self.layout24_3 = QHBoxLayout()
        self.boxes24_3 = self.createNumEdit(self.layout24_3, 24, 40)
        self.ytTot = self.createNumEdit(self.layout24_3, 1, 50)
        self.teLayout.addLayout(self.layout24_3)

        self.layout24_4 = QHBoxLayout()
        self.boxes24_4 = self.createNumEdit(self.layout24_4, 24, 40)
        self.exTot = self.createNumEdit(self.layout24_4, 1, 50)
        self.teLayout.addLayout(self.layout24_4)

        self.hoursLayout.addLayout(self.labelLayout)
        self.hoursLayout.addLayout(self.teLayout)

        self.detailsLayout = QHBoxLayout()

        self.btDetailsLayout = QHBoxLayout()

        self.btDetailsLabelsLayout = QVBoxLayout()
        self.btDL0A = self.createLabelCenteredWithSize("BT %", self.btDetailsLabelsLayout, 50)
        self.btDL1 = self.createLabelCenteredWithSize("00-04", self.btDetailsLabelsLayout, 50)
        self.btDL2 = self.createLabelCenteredWithSize("04-09", self.btDetailsLabelsLayout, 50)
        self.btDL2 = self.createLabelCenteredWithSize("09-14", self.btDetailsLabelsLayout, 50)

        self.btDetailsValuesLayout = QVBoxLayout()

        self.btDVL = QHBoxLayout()
        self.btDVL1 = self.createLabelCentered("Sl1", self.btDVL)
        self.btDVL2 = self.createLabelCentered("Sl2", self.btDVL)
        self.btDVL3 = self.createLabelCentered("Sl3", self.btDVL)

        self.btDetailsValuesLayout_1 = QHBoxLayout()
        self.btDetailsValues1 = self.createNumEdit(self.btDetailsValuesLayout_1, 3, 40)

        self.btDetailsValuesLayout_2 = QHBoxLayout()
        self.btDetailsValues2 = self.createNumEdit(self.btDetailsValuesLayout_2, 3, 40)

        self.btDetailsValuesLayout_3 = QHBoxLayout()
        self.btDetailsValues3 = self.createNumEdit(self.btDetailsValuesLayout_3, 3, 40)

        self.btDetailsValuesLayout.addLayout(self.btDVL)
        self.btDetailsValuesLayout.addLayout(self.btDetailsValuesLayout_1)
        self.btDetailsValuesLayout.addLayout(self.btDetailsValuesLayout_2)
        self.btDetailsValuesLayout.addLayout(self.btDetailsValuesLayout_3)

        self.btDetailsLayout.addLayout(self.btDetailsLabelsLayout)
        self.btDetailsLayout.addLayout(self.btDetailsValuesLayout)

        self.ytDetailsLayout = QHBoxLayout()

        self.ytDetailsLabelsLayout = QVBoxLayout()
        self.ytDL0 = self.createLabelCenteredWithSize("YT %", self.ytDetailsLabelsLayout, 50)
        self.ytDL1 = self.createLabelCenteredWithSize("00-08", self.ytDetailsLabelsLayout, 50)
        self.ytDL2 = self.createLabelCenteredWithSize("08-16", self.ytDetailsLabelsLayout, 50)
        self.ytDL3 = self.createLabelCenteredWithSize("16-00", self.ytDetailsLabelsLayout, 50)

        self.ytDetailsValuesLayout = QVBoxLayout()
        self.ytDVL = QHBoxLayout()
        self.ytDVL1 = self.createLabelCentered("Sl1", self.ytDVL)
        self.ytDVL2 = self.createLabelCentered("Sl2", self.ytDVL)
        self.ytDVL3 = self.createLabelCentered("Sl3", self.ytDVL)

        self.ytDetailsValuesLayout_1 = QHBoxLayout()
        self.ytDetailsValues1 = self.createNumEdit(self.ytDetailsValuesLayout_1, 3, 40)

        self.ytDetailsValuesLayout_2 = QHBoxLayout()
        self.ytDetailsValues2 = self.createNumEdit(self.ytDetailsValuesLayout_2, 3, 40)

        self.ytDetailsValuesLayout_3 = QHBoxLayout()
        self.ytDetailsValues3 = self.createNumEdit(self.ytDetailsValuesLayout_3, 3, 40)

        self.ytDetailsValuesLayout.addLayout(self.ytDVL)
        self.ytDetailsValuesLayout.addLayout(self.ytDetailsValuesLayout_1)
        self.ytDetailsValuesLayout.addLayout(self.ytDetailsValuesLayout_2)
        self.ytDetailsValuesLayout.addLayout(self.ytDetailsValuesLayout_3)

        self.ytDetailsLayout.addLayout(self.ytDetailsLabelsLayout)
        self.ytDetailsLayout.addLayout(self.ytDetailsValuesLayout)

        self.exDetailsLayout = QVBoxLayout()

        # self.exDetailsLabelsLayout = QVBoxLayout()
        # self.exDL0 = self.createLabelCentered("EX %", self.exDetailsLabelsLayout)
        # self.exDL1 = self.createLabelCentered("00-24", self.exDetailsLabelsLayout)

        self.exDetailsValuesLayout = QVBoxLayout()

        self.exDVL = QHBoxLayout()
        self.exDVL0 = self.createLabelCentered("w1h", self.exDVL)
        self.exDVL1 = self.createLabelCentered("+1h", self.exDVL)
        self.exDVL2 = self.createLabelCentered("+2h", self.exDVL)
        self.exDVL3 = self.createLabelCentered("+3h", self.exDVL)
        for i in range(8):
            self.exEmpty = self.createLabelCentered("", self.exDVL)

        self.exDetailsValuesLayout_1 = QHBoxLayout()
        self.exDetailsValues1 = self.createNumEdit(self.exDetailsValuesLayout_1, 4, 40)
        for i in range(8):
            self.exEmpty = self.createLabel("", self.exDetailsValuesLayout_1)

        self.exDetailsValuesLayout.addLayout(self.exDVL)
        self.exDetailsValuesLayout.addLayout(self.exDetailsValuesLayout_1)

        # self.exDetailsLayout.addLayout(self.exDetailsLabelsLayout)
        self.exDetailsLayout.addLayout(self.exDetailsValuesLayout)
        self.exDetailsLayout.addLayout(self.coefsLayout)

        self.detailsLayout.addLayout(self.btDetailsLayout)
        self.detailsLayout.addLayout(self.ytDetailsLayout)
        self.detailsLayout.addLayout(self.exDetailsLayout)
        self.simDataLayout.addLayout(self.emptyLayout)

        self.simDataLayout.addLayout(self.hoursLayout)
        self.simDataLayout.addLayout(self.emptyLayout)
        self.simDataLayout.addLayout(self.detailsLayout)
        self.simDataLayout.addLayout(self.emptyLayout)

        self.mainLayout.addLayout(self.areaSelectionLayout)
        self.mainLayout.addLayout(self.screenButtonsLayout)
        self.mainLayout.addLayout(self.simDataLayout)
        self.mainLayout.addLayout(self.controlButtonsWithDaysLayout)

        self.setLayout(self.mainLayout)
        self.setWindowTitle("HX Delivery Simulator")
        self.manuelChangedCBId = -1;
        self.resetDistrict()

        now = datetime.datetime.now()
        self.begDate.setDate(now)
        self.endDate.setDate(now)

        self.messageBoxSuccess = QMessageBox()
        self.messageBoxSuccess.setIcon(QMessageBox.Information)
        self.messageBoxSuccess.setWindowTitle("Info")
        self.messageBoxSuccess.setText("Successfully completed!")
        self.messageBoxSuccess.setStandardButtons(QMessageBox.Ok)

        self.messageBoxFailure = QMessageBox()
        self.messageBoxFailure.setIcon(QMessageBox.Critical)
        self.messageBoxFailure.setWindowTitle("Error")
        self.messageBoxFailure.setText("Exception occurred!")
        self.messageBoxFailure.setStandardButtons(QMessageBox.Ok)

        self.setEditableFields()

        self.masterCoef[0].setValue(1.0)
        for i in range(7):
            self.dayCoefs[i].setValue(1.0)

        self.stdTot[0].setDisabled(True)
        self.btTot[0].setDisabled(True)
        self.ytTot[0].setDisabled(True)
        self.exTot[0].setDisabled(True)

    def debugMsg(self):
        self.debugCounter += 1
        print("Debug msg : " + str(self.debugCounter))

    def saveToExcel(self):
        print("Save to excel")

    def createLabel(self, name, layout):
        qLabel = QLabel(name)
        qLabel.setMinimumHeight(20)
        qLabel.setMaximumWidth(35)
        qLabel.setFont(self.font_1)
        layout.addWidget(qLabel)
        return qLabel

    def createLabelCentered(self, name, layout):
        qLabel = QLabel(name)
        qLabel.setMaximumHeight(20)
        qLabel.setFont(self.font_1)
        qLabel.setAlignment(QtCore.Qt.AlignCenter)
        layout.addWidget(qLabel)
        return qLabel

    def createLabelCenteredWithSize(self, name, layout, size):
        qLabel = QLabel(name)
        qLabel.setMaximumHeight(15)
        qLabel.setMaximumWidth(size)
        qLabel.setFont(self.font_1)
        qLabel.setAlignment(QtCore.Qt.AlignCenter)
        layout.addWidget(qLabel)
        return qLabel

    def createNumEdit(self, layout, num, size):
        boxes = []
        for i in range(num):
            doubleSpinBox = QDoubleSpinBox()
            doubleSpinBox.setButtonSymbols(QAbstractSpinBox.NoButtons)
            doubleSpinBox.setMaximumWidth(size)
            doubleSpinBox.setRange(0.0, 99999.9);
            doubleSpinBox.setSingleStep(0.1);
            doubleSpinBox.setValue(0.0);
            doubleSpinBox.setAlignment(QtCore.Qt.AlignRight)
            boxes.append(doubleSpinBox)
            layout.addWidget(doubleSpinBox)
        return boxes

    def loadFromExcel(self):
        fileName, _ = QFileDialog.getOpenFileName(self, 'Open file',
                                                  os.getcwd() + "//input//", "Excel files (*.xlsx)")
        if not fileName:
            return
        success = True
        try:
            simDataXls = pd.ExcelFile(fileName)
            self.resetAll()
            percents = simDataXls.parse("percents")
            stdPercentsArr = []
            for i in range(len(percents)):
                stdPercentsArr.append(percents.iloc[i, 1])
            stdPercentsArr = normalizeArr(stdPercentsArr)

            bt1PercentsArr = []
            for i in range(4):
                bt1PercentsArr.append(percents.iloc[i, 3])
            bt1PercentsArr = normalizeArr(bt1PercentsArr)

            bt2PercentsArr = []
            for i in range(9):
                bt2PercentsArr.append(percents.iloc[i, 4])
            bt2PercentsArr = normalizeArr(bt2PercentsArr)

            bt3PercentsArr = []
            for i in range(14):
                bt3PercentsArr.append(percents.iloc[i, 5])
            bt3PercentsArr = normalizeArr(bt3PercentsArr)

            yt1PercentsArr = []
            for i in range(24):
                yt1PercentsArr.append(percents.iloc[i, 6])
            yt1PercentsArr = normalizeArr(yt1PercentsArr)

            yt2PercentsArr = []
            for i in range(24):
                yt2PercentsArr.append(percents.iloc[i, 7])
            yt2PercentsArr = normalizeArr(yt2PercentsArr)

            yt3PercentsArr = []
            for i in range(24):
                yt3PercentsArr.append(percents.iloc[i, 8])
            yt3PercentsArr = normalizeArr(yt3PercentsArr)

            coefficients = simDataXls.parse("coefficients")

            simDataForDistricts = simDataXls.parse("deliveries")
            for i in range(len(simDataForDistricts)):
                dataForDistrict = {}
                districtId = simDataForDistricts.iloc[i, 0]
                stdHour = simDataForDistricts.iloc[i, 1]
                bt1Hour = simDataForDistricts.iloc[i, 2]
                bt2Hour = simDataForDistricts.iloc[i, 3]
                bt3Hour = simDataForDistricts.iloc[i, 4]
                yt1Hour = simDataForDistricts.iloc[i, 5]
                yt2Hour = simDataForDistricts.iloc[i, 6]
                yt3Hour = simDataForDistricts.iloc[i, 7]
                std = []
                bt = []
                bt1 = []
                bt2 = []
                bt3 = []
                yt = []
                yt1 = []
                yt2 = []
                yt3 = []
                ex = []
                exw1 = []
                exp1 = []
                exp2 = []
                exp3 = []
                for i in range(24):
                    std.append(stdHour * stdPercentsArr[i])
                    if i < 4:
                        bt1.append(bt1Hour * bt1PercentsArr[i])
                    else:
                        bt1.append(0)
                    if i < 9:
                        bt2.append(bt2Hour * bt2PercentsArr[i])
                    else:
                        bt2.append(0)
                    if i < 14:
                        bt3.append(bt3Hour * bt3PercentsArr[i])
                    else:
                        bt3.append(0)
                    yt1.append(yt1Hour * yt1PercentsArr[i])
                    yt2.append(yt2Hour * yt2PercentsArr[i])
                    yt3.append(yt3Hour * yt3PercentsArr[i])
                    exw1.append(0)
                    exp1.append(0)
                    exp2.append(0)
                    exp3.append(0)
                for i in range(24):
                    bt.append(bt1[i] + bt2[i] + bt3[i])
                    yt.append(yt1[i] + yt2[i] + yt3[i])
                    ex.append(exw1[i] + exp1[i] + exp2[i] + exp3[i])
                dataForDistrict["std"] = std
                dataForDistrict["bt"] = bt
                dataForDistrict["bt1"] = bt1
                dataForDistrict["bt2"] = bt2
                dataForDistrict["bt3"] = bt3
                dataForDistrict["yt"] = yt
                dataForDistrict["yt1"] = yt1
                dataForDistrict["yt2"] = yt2
                dataForDistrict["yt3"] = yt3
                dataForDistrict["ex"] = ex
                dataForDistrict["exw1"] = exw1
                dataForDistrict["exp1"] = exp1
                dataForDistrict["exp2"] = exp2
                dataForDistrict["exp3"] = exp3

                exCoefBt = coefficients.iloc[0, 1]
                exCoef = coefficients.iloc[1, 1]

                if exCoefBt > 0 or exCoef > 0:
                    exHoursArr = []
                    for i in range(len(percents)):
                        exHourVal = percents.iloc[i, 2]
                        exHoursArr.append(exHourVal)
                    exHoursArr = normalizeArr(exHoursArr)

                    exSlotsArr = []
                    for i in range(2, 6):
                        exSlotsArr.append(coefficients.iloc[i, 1])
                    exSlotsArr = normalizeArr(exSlotsArr)
                    self.createExDeliveriesForDistrict(dataForDistrict, exCoefBt, exCoef, exHoursArr, exSlotsArr)

                self.calcPercents(dataForDistrict)
                self.simData["districtScenarios"][str(districtId)] = dataForDistrict
            self.calcTotalData()
            self.updateScreenByData()
        except:
            success = False
        if (not success):
            self.messageBoxFailure.exec_()

    def calcPercentsBt(self, dataForDistrict):
        btPercents = []
        for i in range(3):
            for j in range(3):
                btPercents.append(0)
        hourBt1 = [0, 0, 0]
        hourBt2 = [0, 0, 0]
        hourBt3 = [0, 0, 0]
        hourBt1Tot = 0
        hourBt2Tot = 0
        hourBt3Tot = 0
        for i in range(14):
            if i < 4:
                hourBt1Tot += dataForDistrict["bt"][i]
                hourBt1[0] += dataForDistrict["bt1"][i]
                hourBt1[1] += dataForDistrict["bt2"][i]
                hourBt1[2] += dataForDistrict["bt3"][i]
            elif i < 9:
                hourBt2Tot += dataForDistrict["bt"][i]
                hourBt2[1] += dataForDistrict["bt2"][i]
                hourBt2[2] += dataForDistrict["bt3"][i]
            else:
                hourBt3Tot += dataForDistrict["bt"][i]
                hourBt3[2] += dataForDistrict["bt3"][i]
        if hourBt1Tot > 0:
            hourBt1RateRes = [x / hourBt1Tot for x in hourBt1]
        else:
            hourBt1RateRes = [0, 0, 0]
        if hourBt2Tot > 0:
            hourBt2RateRes = [x / hourBt2Tot for x in hourBt2]
        else:
            hourBt2RateRes = [0, 0, 0]
        if hourBt3Tot > 0:
            hourBt3RateRes = [x / hourBt3Tot for x in hourBt3]
        else:
            hourBt3RateRes = [0, 0, 0]
        for i in range(3):
            btPercents[i] = round(hourBt1RateRes[i] * 100, 2)
        for i in range(3):
            btPercents[i + 3] = round(hourBt2RateRes[i] * 100, 2)
        for i in range(3):
            btPercents[i + 6] = round(hourBt3RateRes[i] * 100, 2)
        dataForDistrict["btPercents"] = btPercents

    def calcPercentsYt(self, dataForDistrict):
        ytPercents = []
        for i in range(3):
            for j in range(3):
                ytPercents.append(0)
        hourYt1 = [0, 0, 0]
        hourYt2 = [0, 0, 0]
        hourYt3 = [0, 0, 0]
        hourYt1Tot = 0
        hourYt2Tot = 0
        hourYt3Tot = 0
        for i in range(24):
            if i < 8:
                hourYt1Tot += dataForDistrict["yt"][i]
                hourYt1[0] += dataForDistrict["yt1"][i]
                hourYt1[1] += dataForDistrict["yt2"][i]
                hourYt1[2] += dataForDistrict["yt3"][i]
            elif i < 16:
                hourYt2Tot += dataForDistrict["yt"][i]
                hourYt2[0] += dataForDistrict["yt1"][i]
                hourYt2[1] += dataForDistrict["yt2"][i]
                hourYt2[2] += dataForDistrict["yt3"][i]
            else:
                hourYt3Tot += dataForDistrict["yt"][i]
                hourYt3[0] += dataForDistrict["yt1"][i]
                hourYt3[1] += dataForDistrict["yt2"][i]
                hourYt3[2] += dataForDistrict["yt3"][i]
        if hourYt1Tot > 0:
            hourYt1RateRes = [x / hourYt1Tot for x in hourYt1]
        else:
            hourYt1RateRes = [0, 0, 0]
        if hourYt2Tot > 0:
            hourYt2RateRes = [x / hourYt2Tot for x in hourYt2]
        else:
            hourYt2RateRes = [0, 0, 0]
        if hourYt3Tot > 0:
            hourYt3RateRes = [x / hourYt3Tot for x in hourYt3]
        else:
            hourYt3RateRes = [0, 0, 0]
        for i in range(3):
            ytPercents[i] = round(hourYt1RateRes[i] * 100, 2)
        for i in range(3):
            ytPercents[i + 3] = round(hourYt2RateRes[i] * 100, 2)
        for i in range(3):
            ytPercents[i + 6] = round(hourYt3RateRes[i] * 100, 2)
        dataForDistrict["ytPercents"] = ytPercents

    def calcPercentsEx(self, dataForDistrict):
        exPercents = [0, 0, 0, 0]
        hourEx1 = [0, 0, 0, 0]
        hourEx1Tot = 0
        for i in range(24):
            hourEx1Tot += dataForDistrict["ex"][i]
            hourEx1[0] += dataForDistrict["exw1"][i]
            hourEx1[1] += dataForDistrict["exp1"][i]
            hourEx1[2] += dataForDistrict["exp2"][i]
            hourEx1[3] += dataForDistrict["exp3"][i]
        if hourEx1Tot > 0:
            hourEx1RateRes = [x / hourEx1Tot for x in hourEx1]
        else:
            hourEx1RateRes = [0, 0, 0, 0]
        for i in range(4):
            exPercents[i] = round(hourEx1RateRes[i] * 100, 2)
        dataForDistrict["exPercents"] = exPercents

    def calcPercents(self, dataForDistrict):
        self.calcPercentsBt(dataForDistrict)
        self.calcPercentsYt(dataForDistrict)
        self.calcPercentsEx(dataForDistrict)

    def loadFromJson(self):
        success = True
        fileName, _ = QFileDialog.getOpenFileName(self, 'Open file',
                                                  os.getcwd() + "//input//", "json files (*.json)")
        if not fileName:
            return
        try:
            with open(fileName) as f:
                jsonData = json.load(f)
            self.resetAll()
            self.getDatesFromJson(jsonData)
            self.getCoefsFromJson(jsonData)
            districtScenarios = jsonData["districtScenarios"]
            for districtId in districtScenarios.keys():
                districtScenario = districtScenarios.get(districtId)
                self.fillDistrict(districtId, districtScenario)
            self.debugMsg()
            self.calcTotalData()
            self.debugMsg()
            self.updateScreenByData()
            self.debugMsg()
        except:
            success = False
        if (not success):
            self.messageBoxFailure.exec_()
            return

    def getDatesFromJson(self, jsonData):
        self.begDate.setDate(createDateFromDateJson(jsonData.get("begDate")))
        self.endDate.setDate(createDateFromDateJson(jsonData.get("endDate")))

    def getCoefsFromJson(self, jsonData):
        coefs = jsonData.get("coefs")
        self.masterCoef[0].setValue(coefs.get("masterCoef"))
        dayCoefs = coefs.get("dayCoefs")
        for i in range(7):
            self.dayCoefs[i].setValue(dayCoefs[i])

    def fillDistrict(self, districtId, districtScenario):
        dataForDistrict = {}
        std = []
        bt = []
        yt = []
        ex = []
        for i in range(24):
            std.append(districtScenario.get("std")[i])
            if i < 14:
                bt.append(districtScenario.get("bt")[i])
            else:
                bt.append(0)
            yt.append(districtScenario.get("yt")[i])
            ex.append(districtScenario.get("ex")[i])
        dataForDistrict["std"] = std
        dataForDistrict["bt"] = bt
        dataForDistrict["yt"] = yt
        dataForDistrict["ex"] = ex
        btPercents = []
        for i in range(9):
            btPercents.append(districtScenario.get("btPercents")[i])
        dataForDistrict["btPercents"] = btPercents
        ytPercents = []
        for i in range(9):
            ytPercents.append(districtScenario.get("ytPercents")[i])
        dataForDistrict["ytPercents"] = ytPercents
        exPercents = []
        for i in range(4):
            exPercents.append(districtScenario.get("exPercents")[i])
        dataForDistrict["exPercents"] = exPercents
        self.simData["districtScenarios"][str(districtId)] = dataForDistrict

    def closeButton(self):
        self.close()

    def generateDistrictButton(self):
        self.generateSimDataForDistrict(self.activeDistrictId)

    def generateAllButton(self):
        self.generateSimDataForDistrict()

    def createExDeliveriesButton(self):
        self.createExDeliveries()
        self.calcTotalData()
        self.updateScreenByData()

    def testScenarioButton(self):
        fileName, _ = QFileDialog.getOpenFileName(self, 'Open file',
                                                  os.getcwd() + "//input//", "Excel files (*.xlsx)")
        if not fileName:
            return
        success = True
        try:
            simDataXls = pd.ExcelFile(fileName)
            testDistrictSplit(simDataXls, self.simData)
        except:
            success = False
        if (not success):
            self.messageBoxFailure.exec_()

    def createExDeliveriesForDistrict(self, dataForDistrict, exCoefBt, exCoef, exHourPercents, exSlotPercents):
        ex = []
        exw1 = []
        exp1 = []
        exp2 = []
        exp3 = []

        totExpected = 0

        if exCoefBt > 0:
            for i in range(24):
                totExpected += dataForDistrict.get("bt")[i]
                totExpected += dataForDistrict.get("yt")[i]
            totExpected *= exCoefBt
        else:
            totExpected = exCoef / self.numDistricts

        for i in range(24):
            val = totExpected * exHourPercents[i]
            exw1.append(val * exSlotPercents[0])
            exp1.append(val * exSlotPercents[1])
            exp2.append(val * exSlotPercents[2])
            exp3.append(val * exSlotPercents[3])
        for i in range(24):
            ex.append(exw1[i] + exp1[i] + exp2[i] + exp3[i])

        dataForDistrict["ex"] = ex
        dataForDistrict["exw1"] = exw1
        dataForDistrict["exp1"] = exp1
        dataForDistrict["exp2"] = exp2
        dataForDistrict["exp3"] = exp3

    def createExDeliveries(self, exCoefBt, exCoef, exHourPercents, exSlotPercents):
        districtScenarios = self.simData.get("districtScenarios")
        for districtId in districtScenarios.keys():
            if (int(districtId) == ALL_DISTRICTS):
                continue
            dataForDistrict = districtScenarios.get(districtId)
            self.createExDeliveriesForDistrict(dataForDistrict, exCoefBt, exCoef, exHourPercents, exSlotPercents)
            self.calcPercentsEx(dataForDistrict)
            self.simData["districtScenarios"][districtId] = dataForDistrict

    def generateSimDataForDistrict(self, districtId=0):
        self.saveDistrictDataFromInterface(self.activeDistrictId)
        self.setDatesToJson()
        self.setCoefsToJson()
        success = False
        try:
            success = generateDeliverySimsFromSimData(self.simData, districtId)
        except:
            success = False
        if (success):
            self.messageBoxSuccess.exec_()
        else:
            self.messageBoxFailure.exec_()

    def setDatesToJson(self):
        self.simData["begDate"] = createDateJsonFromDate(self.begDate.date())
        self.simData["endDate"] = createDateJsonFromDate(self.endDate.date())

    def resetAllButton(self):
        self.resetAll()

    def resetAll(self):
        success = True
        try:
            self.simData["districtScenarios"] = {}
            self.calcTotalData()
            self.updateScreenByData()
        except:
            success = False

        if not success:
            self.messageBoxFailure.exec_()

    def resetDistrictButton(self):
        if self.activeDistrictId == ALL_DISTRICTS:
            return
        success = True
        try:
            self.resetDistrict()
        except:
            success = False
        if (not success):
            self.messageBoxFailure.exec_()

    def saveButton(self):
        self.saveDistrictDataFromInterface(self.activeDistrictId)
        self.setDatesToJson()
        self.setCoefsToJson()
        success = True
        try:
            writeJsonToFileLocal(json.dumps(self.simData), "simData")
        except:
            success = False
        if (success):
            self.messageBoxSuccess.exec_()
        else:
            self.messageBoxFailure.exec_()

    def setCoefsToJson(self):
        coefs = {}
        coefs["masterCoef"] = self.masterCoef[0].value()
        dayCoefs = []
        for i in range(7):
            dayCoefs.append(self.dayCoefs[i].value())
        coefs["dayCoefs"] = dayCoefs
        self.simData["coefs"] = coefs

    def districtChange(self, i):
        oldDistrictId = self.activeDistrictId

        if i != 0:
            newDistrictId = self.simData["districtList"][i]["id"]
        else:
            newDistrictId = ALL_DISTRICTS
        self.saveDistrictDataFromInterface(oldDistrictId)
        if newDistrictId == ALL_DISTRICTS:
            self.calcTotalData()

        self.activeDistrictId = newDistrictId
        self.updateScreenByData()
        self.setEditableFields()

    def saveDistrictDataFromInterface(self, districtId):
        if self.activeDistrictId == ALL_DISTRICTS:
            return

        dataForDistrict = {}
        std = []
        bt = []
        yt = []
        ex = []
        for i in range(24):
            std.append(self.boxes24_1[i].value())
            bt.append(self.boxes24_2[i].value())
            yt.append(self.boxes24_3[i].value())
            ex.append(self.boxes24_4[i].value())
        dataForDistrict["std"] = std
        dataForDistrict["bt"] = bt
        dataForDistrict["yt"] = yt
        dataForDistrict["ex"] = ex

        btPercents = []
        for i in range(3):
            btPercents.append(self.btDetailsValues1[i].value())
        for i in range(3):
            btPercents.append(self.btDetailsValues2[i].value())
        for i in range(3):
            btPercents.append(self.btDetailsValues3[i].value())
        dataForDistrict["btPercents"] = btPercents

        ytPercents = []
        for i in range(3):
            ytPercents.append(self.ytDetailsValues1[i].value())
        for i in range(3):
            ytPercents.append(self.ytDetailsValues2[i].value())
        for i in range(3):
            ytPercents.append(self.ytDetailsValues3[i].value())
        dataForDistrict["ytPercents"] = ytPercents

        exPercents = []
        for i in range(4):
            exPercents.append(self.exDetailsValues1[i].value())
        dataForDistrict["exPercents"] = exPercents

        self.simData["districtScenarios"][str(districtId)] = dataForDistrict

    def updateScreenByData(self):
        if str(self.activeDistrictId) in self.simData["districtScenarios"].keys():
            districtData = self.simData["districtScenarios"][str(self.activeDistrictId)]
            self.updateScreenHours(districtData["std"], self.boxes24_1)
            self.updateScreenHours(districtData["bt"], self.boxes24_2)
            self.updateScreenHours(districtData["yt"], self.boxes24_3)
            self.updateScreenHours(districtData["ex"], self.boxes24_4)

            self.updateHourTotals()

            for i in range(3):
                self.btDetailsValues1[i].setValue(districtData["btPercents"][i])
            for i in range(3):
                self.btDetailsValues2[i].setValue(districtData["btPercents"][i + 3])
            for i in range(3):
                self.btDetailsValues3[i].setValue(districtData["btPercents"][i + 6])

            for i in range(3):
                self.ytDetailsValues1[i].setValue(districtData["ytPercents"][i])
            for i in range(3):
                self.ytDetailsValues2[i].setValue(districtData["ytPercents"][i + 3])
            for i in range(3):
                self.ytDetailsValues3[i].setValue(districtData["ytPercents"][i + 6])

            for i in range(4):
                self.exDetailsValues1[i].setValue(districtData["exPercents"][i])

        else:
            self.resetDistrict()

    def updateHourTotals(self):
        stdTot = 0
        btTot = 0
        ytTot = 0
        exTot = 0
        for i in range(24):
            stdTot += self.boxes24_1[i].value()
            btTot += self.boxes24_2[i].value()
            ytTot += self.boxes24_3[i].value()
            exTot += self.boxes24_4[i].value()
        self.stdTot[0].setValue(stdTot)
        self.btTot[0].setValue(btTot)
        self.ytTot[0].setValue(ytTot)
        self.exTot[0].setValue(exTot)

    def resetDistrict(self):
        for i in range(24):
            self.boxes24_1[i].setValue(0)
            self.boxes24_2[i].setValue(0)
            self.boxes24_3[i].setValue(0)
            self.boxes24_4[i].setValue(0)

        for i in range(3):
            self.btDetailsValues1[i].setValue(33.33)

        self.btDetailsValues2[0].setValue(0.0)
        self.btDetailsValues2[1].setValue(50.0)
        self.btDetailsValues2[2].setValue(50.0)
        self.btDetailsValues3[0].setValue(0.0)
        self.btDetailsValues3[1].setValue(0.0)
        self.btDetailsValues3[2].setValue(100.0)

        for i in range(3):
            self.ytDetailsValues1[i].setValue(33.33)
        for i in range(3):
            self.ytDetailsValues2[i].setValue(33.33)
        for i in range(3):
            self.ytDetailsValues3[i].setValue(33.33)

        for i in range(4):
            self.exDetailsValues1[i].setValue(25.00)

    def updateScreenHours(self, districtData, boxList):
        for i in range(24):
            boxList[i].setValue(districtData[i])

    def createHourLabels(self):
        self.createLabelCentered("00-01", self.hours)
        self.createLabelCentered("01-02", self.hours)
        self.createLabelCentered("02-03", self.hours)
        self.createLabelCentered("03-04", self.hours)
        self.createLabelCentered("04-05", self.hours)
        self.createLabelCentered("05-06", self.hours)
        self.createLabelCentered("06-07", self.hours)
        self.createLabelCentered("07-08", self.hours)
        self.createLabelCentered("08-09", self.hours)
        self.createLabelCentered("09-10", self.hours)
        self.createLabelCentered("10-11", self.hours)
        self.createLabelCentered("11-12", self.hours)
        self.createLabelCentered("12-13", self.hours)
        self.createLabelCentered("13-14", self.hours)
        self.createLabelCentered("14-15", self.hours)
        self.createLabelCentered("15-16", self.hours)
        self.createLabelCentered("16-17", self.hours)
        self.createLabelCentered("17-18", self.hours)
        self.createLabelCentered("18-19", self.hours)
        self.createLabelCentered("19-20", self.hours)
        self.createLabelCentered("20-21", self.hours)
        self.createLabelCentered("21-22", self.hours)
        self.createLabelCentered("22-23", self.hours)
        self.createLabelCentered("23-00", self.hours)
        self.createLabelCentered("Tot", self.hours)

    def calcTotalData(self):
        totalHoursStd = []
        totalHoursBt = []
        totalHoursYt = []
        totalHoursEx = []
        for i in range(24):
            totalHoursStd.append(0)
            totalHoursBt.append(0)
            totalHoursYt.append(0)
            totalHoursEx.append(0)
        btPercents = []
        ytPercents = []
        exPercents = []
        for i in range(9):
            btPercents.append(0)
        for i in range(9):
            ytPercents.append(0)
        for i in range(4):
            exPercents.append(0)
        hourBt1Tot = 0
        hourBt2Tot = 0
        hourBt3Tot = 0
        hourBt1RateTot = []
        hourBt2RateTot = []
        hourBt3RateTot = []
        hourYt1Tot = 0
        hourYt2Tot = 0
        hourYt3Tot = 0
        hourYt1RateTot = []
        hourYt2RateTot = []
        hourYt3RateTot = []
        for i in range(3):
            hourBt1RateTot.append(0)
            hourBt2RateTot.append(0)
            hourBt3RateTot.append(0)
            hourYt1RateTot.append(0)
            hourYt2RateTot.append(0)
            hourYt3RateTot.append(0)
        hourEx1Tot = 0
        hourEx1RateTot = []
        for i in range(4):
            hourEx1RateTot.append(0)
        districtScenarios = self.simData.get("districtScenarios")
        for districtId in districtScenarios.keys():
            if int(districtId) != ALL_DISTRICTS:
                districtScenario = districtScenarios.get(districtId)
                totalHoursStd = list(map(add, totalHoursStd, districtScenario.get("std")))
                totalHoursBt = list(map(add, totalHoursBt, districtScenario.get("bt")))
                totalHoursYt = list(map(add, totalHoursYt, districtScenario.get("yt")))
                totalHoursEx = list(map(add, totalHoursEx, districtScenario.get("ex")))
                hoursBt = districtScenario.get("bt")
                percentsBt = districtScenario.get("btPercents")
                hourBt1 = 0
                hourBt2 = 0
                hourBt3 = 0
                for i in range(0, 24):
                    if i < 4:
                        hourBt1 += hoursBt[i]
                    elif i < 9:
                        hourBt2 += hoursBt[i]
                    else:
                        hourBt3 += hoursBt[i]
                hourBt1Tot += hourBt1
                hourBt2Tot += hourBt2
                hourBt3Tot += hourBt3
                hourBt1Rate = []
                hourBt2Rate = []
                hourBt3Rate = []
                for i in range(3):
                    hourBt1Rate.append(hourBt1 * percentsBt[i] * 0.01)
                for i in range(3):
                    hourBt2Rate.append(hourBt2 * percentsBt[i + 3] * 0.01)
                for i in range(3):
                    hourBt3Rate.append(hourBt3 * percentsBt[i + 6] * 0.01)
                hourBt1RateTot = list(map(add, hourBt1RateTot, hourBt1Rate))
                hourBt2RateTot = list(map(add, hourBt2RateTot, hourBt2Rate))
                hourBt3RateTot = list(map(add, hourBt3RateTot, hourBt3Rate))
                if hourBt1Tot > 0:
                    hourBt1RateRes = [x / hourBt1Tot for x in hourBt1RateTot]
                else:
                    hourBt1RateRes = [0, 0, 0]
                if hourBt2Tot > 0:
                    hourBt2RateRes = [x / hourBt2Tot for x in hourBt2RateTot]
                else:
                    hourBt2RateRes = [0, 0, 0]
                if hourBt3Tot > 0:
                    hourBt3RateRes = [x / hourBt3Tot for x in hourBt3RateTot]
                else:
                    hourBt3RateRes = [0, 0, 0]
                for i in range(3):
                    btPercents[i] = round(hourBt1RateRes[i] * 100, 2)
                for i in range(3):
                    btPercents[i + 3] = round(hourBt2RateRes[i] * 100, 2)
                for i in range(3):
                    btPercents[i + 6] = round(hourBt3RateRes[i] * 100, 2)
                hoursYt = districtScenario.get("yt")
                percentsYt = districtScenario.get("ytPercents")
                hourYt1 = 0
                hourYt2 = 0
                hourYt3 = 0
                for i in range(0, 24):
                    if i < 8:
                        hourYt1 += hoursYt[i]
                    elif i < 16:
                        hourYt2 += hoursYt[i]
                    else:
                        hourYt3 += hoursYt[i]
                hourYt1Tot += hourYt1
                hourYt2Tot += hourYt2
                hourYt3Tot += hourYt3
                hourYt1Rate = []
                hourYt2Rate = []
                hourYt3Rate = []
                for i in range(3):
                    hourYt1Rate.append(hourYt1 * percentsYt[i] * 0.01)
                for i in range(3):
                    hourYt2Rate.append(hourYt2 * percentsYt[i + 3] * 0.01)
                for i in range(3):
                    hourYt3Rate.append(hourYt3 * percentsYt[i + 6] * 0.01)
                hourYt1RateTot = list(map(add, hourYt1RateTot, hourYt1Rate))
                hourYt2RateTot = list(map(add, hourYt2RateTot, hourYt2Rate))
                hourYt3RateTot = list(map(add, hourYt3RateTot, hourYt3Rate))
                if hourYt1Tot > 0:
                    hourYt1RateRes = [x / hourYt1Tot for x in hourYt1RateTot]
                else:
                    hourYt1RateRes = [0, 0, 0]
                if hourYt2Tot > 0:
                    hourYt2RateRes = [x / hourYt2Tot for x in hourYt2RateTot]
                else:
                    hourYt2RateRes = [0, 0, 0]
                if hourYt3Tot > 0:
                    hourYt3RateRes = [x / hourYt3Tot for x in hourYt3RateTot]
                else:
                    hourYt3RateRes = [0, 0, 0]
                for i in range(3):
                    ytPercents[i] = round(hourYt1RateRes[i] * 100, 2)
                for i in range(3):
                    ytPercents[i + 3] = round(hourYt2RateRes[i] * 100, 2)
                for i in range(3):
                    ytPercents[i + 6] = round(hourYt3RateRes[i] * 100, 2)
                hoursEx = districtScenario.get("ex")
                percentsEx = districtScenario.get("exPercents")
                hourEx1 = 0
                for i in range(0, 24):
                    hourEx1 += hoursEx[i]
                hourEx1Tot += hourEx1
                hourEx1Rate = []
                for i in range(4):
                    hourEx1Rate.append(hourEx1 * percentsEx[i] * 0.01)
                hourEx1RateTot = list(map(add, hourEx1RateTot, hourEx1Rate))
                if hourEx1Tot > 0:
                    hourEx1RateRes = [x / hourEx1Tot for x in hourEx1RateTot]
                else:
                    hourEx1RateRes = [0, 0, 0, 0]
                for i in range(4):
                    exPercents[i] = round(hourEx1RateRes[i] * 100, 2)

        dataForDistrict = {}
        dataForDistrict["std"] = totalHoursStd
        dataForDistrict["bt"] = totalHoursBt
        dataForDistrict["yt"] = totalHoursYt
        dataForDistrict["ex"] = totalHoursEx

        dataForDistrict["btPercents"] = btPercents
        dataForDistrict["ytPercents"] = ytPercents
        dataForDistrict["exPercents"] = exPercents

        self.simData["districtScenarios"][str(0)] = dataForDistrict

    def setEditableFields(self):
        if (self.activeDistrictId == ALL_DISTRICTS):
            for i in range(24):
                self.boxes24_1[i].setDisabled(True)
                self.boxes24_2[i].setDisabled(True)
                self.boxes24_3[i].setDisabled(True)
                self.boxes24_4[i].setDisabled(True)
                for i in range(3):
                    self.btDetailsValues1[i].setDisabled(True)
                    self.btDetailsValues2[i].setDisabled(True)
                    self.btDetailsValues3[i].setDisabled(True)
                for i in range(3):
                    self.ytDetailsValues1[i].setDisabled(True)
                    self.ytDetailsValues2[i].setDisabled(True)
                    self.ytDetailsValues3[i].setDisabled(True)
                for i in range(4):
                    self.exDetailsValues1[i].setDisabled(True)
            self.buttonGenDist.setEnabled(False)
            self.buttonR.setEnabled(False)
        else:
            for i in range(24):
                self.boxes24_1[i].setEnabled(True)
                self.boxes24_2[i].setEnabled(True)
                self.boxes24_3[i].setEnabled(True)
                self.boxes24_4[i].setEnabled(True)
                for i in range(3):
                    self.btDetailsValues1[i].setEnabled(True)
                    self.btDetailsValues2[i].setEnabled(True)
                    self.btDetailsValues3[i].setEnabled(True)
                for i in range(3):
                    self.ytDetailsValues1[i].setEnabled(True)
                    self.ytDetailsValues2[i].setEnabled(True)
                    self.ytDetailsValues3[i].setEnabled(True)
                for i in range(4):
                    self.exDetailsValues1[i].setEnabled(True)
            for i in range(10):
                self.boxes24_2[i + 14].setDisabled(True)
            self.btDetailsValues2[0].setDisabled(True)
            for i in range(3):
                self.btDetailsValues3[i].setDisabled(True)
            self.buttonGenDist.setEnabled(True)
            self.buttonR.setEnabled(True)
