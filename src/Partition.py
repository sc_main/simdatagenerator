def partition(collection):
    if len(collection) == 1:
        yield [ collection ]
        return

    first = collection[0]
    for smaller in partition(collection[1:]):
        # insert `first` in each of the subpartition's subsets
        for n, subset in enumerate(smaller):
            yield smaller[:n] + [[ first ] + subset]  + smaller[n+1:]
        # put `first` in its own subset
        yield [ [ first ] ] + smaller


numList = list(range(1,15))


numVehicles = 4


counter = 0

for n, p in enumerate(partition(numList), 1):
    counter += 1

    # numPathes = len(p)
    # if numPathes == numVehicles:
    #     print("===========")
    #     for i in range(numPathes):
    #         print(p[i])

