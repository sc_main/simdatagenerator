from DateMethods import *
import itertools
import copy

NUM_VEHICLES = 9
DELIVERY_TIME_LIMIT_HOURS = 8
AVG_DELIVERY_TIME_MINUTES = 5
OPTIMIZE_PATHS_AFTER_ADDITION = False
SPLIT_DISTRICTS = True

def testDistrictSplit(simDataXls, simData):
    deliveryTimeLimit = DELIVERY_TIME_LIMIT_HOURS * 3600
    avgDeliveryTime = AVG_DELIVERY_TIME_MINUTES * 60
    districtInfoArr, distanceMatrix = prepareDistictInfoArrAndDistanceMatrix(simData["districtList"], simData["districtDistances"], simDataXls)

    if SPLIT_DISTRICTS:
        districtInfoArr, distanceMatrix = splitDistricts(districtInfoArr, simData["districtDistances"])

    numDistricts = len(districtInfoArr) - 1

    routeProblemInfo = {}
    routeProblemInfo["deliveryTimeLimit"] = deliveryTimeLimit
    routeProblemInfo["avgDeliveryTime"] = avgDeliveryTime
    routeProblemInfo["districtInfoArr"] = districtInfoArr
    routeProblemInfo["distanceMatrix"] = distanceMatrix
    routeProblemInfo["numDistricts"] = numDistricts
    routeProblemInfo["nomineeNodes"] = list(range(1, numDistricts + 1))
    routeProblemInfo["usedNodes"] = []
    routeProblemInfo["numVehicles"] = NUM_VEHICLES
    routeProblemInfo["maxPathTime"] = 0
    routeProblemInfo["minPathTime"] = 0
    routeProblemInfo["totalPathTime"] = 0
    routeProblemInfo["paths"]  = initRoutePaths(routeProblemInfo)
    updateMaxMinPathTimes(routeProblemInfo)

    finished = False

    initMaxMinDistance(routeProblemInfo)

    finished = True
    while len(routeProblemInfo["nomineeNodes"]) > 0 and not finished:
        nodeId = getNodeWithMaxMinDistanceToUsedNodes(routeProblemInfo)
        additionResult = addNodeToRouteNodeId(routeProblemInfo, nodeId, "cheIns")
        if additionResult["feasible"]:
            vehicleId = additionResult["vehicleId"]
            if OPTIMIZE_PATHS_AFTER_ADDITION:
                optimizePath(routeProblemInfo, vehicleId)
        else:
            finished = True

    if len(routeProblemInfo["nomineeNodes"])== 0:
        print("All nodes added!!!")
    else:
        print("Not all nodes added!!!")

    printRoutePaths(routeProblemInfo)
    printPathTimes(routeProblemInfo)

    # nodeId = getNodeIndexByDistrictId(routeProblemInfo, 1430)
    # minDistance, minDistNodeId = findMinDistanceToNode(routeProblemInfo, nodeId)
    # print(minDistance)
    # print(minDistNodeId)
    # print(getDistrictId(routeProblemInfo, minDistNodeId))l


    # nodeList = [30, 28, 33, 7, 25, 38, 37, 9, 34, 49, 46, 41, 40, 6, 45, 4, 1]
    # path = createPathFromNodeList(routeProblemInfo, nodeList)
    # routeProblemInfo["paths"][6] = path

    # nodeList = [6, 4]
    # path = createPathFromNodeList(routeProblemInfo, nodeList)
    # routeProblemInfo["paths"][6] = path

    # for i in range(2):
    #     addNodeToPath(routeProblemInfo, 1)

    # for i in range(routeProblemInfo["numVehicles"]):
    #     addNodeToPath(routeProblemInfo, i)

    # addNodeToPathNodeId(routeProblemInfo, 7, 5)

    # addNodeToRouteNodeId(routeProblemInfo, routeProblemInfo["nomineeNodes"][0], "cheIns")

    # print(getUsedDistrictIds(routeProblemInfo))
    # print(getNomineeDistrictIds(routeProblemInfo))

def findMinDistanceToNode(routeProblemInfo, nodeId):
    minDistance = 99999999
    bestNodeId = -1
    for i in range(len(routeProblemInfo["districtInfoArr"])):
        if i == nodeId:
            continue
        dist = 0
        dist += findDurationByIndex(routeProblemInfo, nodeId, i)
        dist += findDurationByIndex(routeProblemInfo, i, nodeId)
        if dist < minDistance:
            minDistance = dist
            bestNodeId = i
    return minDistance, bestNodeId

def initMaxMinDistance(routeProblemInfo):
    nodeId = getNodeWithMaxDistanceToXdock(routeProblemInfo)
    addNodeToPathNodeId(routeProblemInfo, 0, nodeId)
    for i in range(routeProblemInfo["numVehicles"] - 1):
        nodeId = getNodeWithMaxMinDistanceToUsedNodes(routeProblemInfo)
        addNodeToPathNodeId(routeProblemInfo, i+1, nodeId)

def initMaxTotalDistance(routeProblemInfo):
    nodeId = getNodeWithMaxDistanceToXdock(routeProblemInfo)
    addNodeToPathNodeId(routeProblemInfo, 0, nodeId)
    for i in range(routeProblemInfo["numVehicles"] - 1):
        nodeId = getNodeWithMaxTotalDistanceToUsedNodes(routeProblemInfo)
        addNodeToPathNodeId(routeProblemInfo, i+1, nodeId)

def getNodeWithMaxDistanceToXdock(routeProblemInfo):
    maxDistance = -1
    bestNodeId = -1
    for nodeId in routeProblemInfo["nomineeNodes"]:
        totalDistance = getDistanceToXdock(routeProblemInfo, nodeId)
        if totalDistance > maxDistance:
            maxDistance = totalDistance
            bestNodeId = nodeId
    return bestNodeId

def getDistanceToXdock(routeProblemInfo, nodeId):
    totalDistance = 0
    totalDistance += findDurationByIndex(routeProblemInfo, nodeId, 0)
    totalDistance += findDurationByIndex(routeProblemInfo, 0, nodeId)
    return totalDistance

def getNodeWithMaxTotalDistanceToUsedNodes(routeProblemInfo):
    maxDistance = -1
    bestNodeId = -1
    for nodeId in routeProblemInfo["nomineeNodes"]:
        totalDistance = getTotalDistanceToUsedNodes(routeProblemInfo, nodeId)
        if totalDistance > maxDistance:
            maxDistance = totalDistance
            bestNodeId = nodeId
    return bestNodeId

def getNodeWithMaxMinDistanceToUsedNodes(routeProblemInfo):
    maxMinDistance = -1
    bestNodeId = -1
    for nodeId in routeProblemInfo["nomineeNodes"]:
        minDistance = 99999999
        for usedNodeId in routeProblemInfo["usedNodes"]:
            distanceToUsedNode = 0
            distanceToUsedNode += findDurationByIndex(routeProblemInfo, nodeId, usedNodeId)
            distanceToUsedNode += findDurationByIndex(routeProblemInfo, usedNodeId, nodeId)
            if distanceToUsedNode < minDistance:
                minDistance = distanceToUsedNode
        if minDistance > maxMinDistance:
            maxMinDistance = minDistance
            bestNodeId = nodeId
    return bestNodeId

def getNodeWithMinMinDistanceToUsedNodes(routeProblemInfo):
    minMinDistance = 99999999
    bestNodeId = -1
    for nodeId in routeProblemInfo["nomineeNodes"]:
        minDistance = 99999999
        for usedNodeId in routeProblemInfo["usedNodes"]:
            distanceToUsedNode = 0
            distanceToUsedNode += findDurationByIndex(routeProblemInfo, nodeId, usedNodeId)
            distanceToUsedNode += findDurationByIndex(routeProblemInfo, usedNodeId, nodeId)
            if distanceToUsedNode < minDistance:
                minDistance = distanceToUsedNode
        if minDistance < minMinDistance:
            minMinDistance = minDistance
            bestNodeId = nodeId
    return bestNodeId

def getTotalDistanceToUsedNodes(routeProblemInfo, nodeId):
    totalDistance = 99999999
    for usedNodeId in routeProblemInfo["usedNodes"]:
        totalDistance += findDurationByIndex(routeProblemInfo, nodeId, usedNodeId)
        totalDistance += findDurationByIndex(routeProblemInfo, usedNodeId, nodeId)
    return totalDistance

def updateMaxMinPathTimes(routeProblemInfo):
    maxPathTime, minPathTime, totalPathTime = findPathTimes(routeProblemInfo)
    routeProblemInfo["maxPathTime"] = maxPathTime
    routeProblemInfo["minPathTime"] = minPathTime
    routeProblemInfo["totalPathTime"] = totalPathTime

def findPathTimes(routeProblemInfo):
    maxPathTime = -1
    minPathTime = 99999999
    totalPathTime = 0
    for path in routeProblemInfo["paths"]:
        pathTime = path["time"]
        totalPathTime += pathTime
        if pathTime > maxPathTime:
            maxPathTime = pathTime
        if pathTime < minPathTime:
            minPathTime = pathTime
    return maxPathTime, minPathTime, totalPathTime

def printRoutePaths(routeProblemInfo):
    for i in range(len(routeProblemInfo["paths"])):
        path = routeProblemInfo["paths"][i]
        pathActive = False
        for j in path["nodeList"]:
            if j != 0:
                pathActive = True
                break
        if pathActive:
            feasibleStr = "Feasible"
            if not path["feasible"]:
                feasibleStr = "Not feasible"
            # print("Vehicle " , i , ":" , path["nodeList"] , secondsToHoursStr(path["time"]), feasibleStr)
            print("======================================================")
            print("Path ", i)
            print(getPathDistrictIds(routeProblemInfo, path))
            print(secondsToHoursStr(path["time"]))
            print(path["numDeliveries"])
            print(feasibleStr)

def getPathDistrictIds(routeProblemInfo, path):
    districtIds = []
    for nodeId in path["nodeList"]:
        districtId = routeProblemInfo["districtInfoArr"][nodeId]["id"]
        if districtId == -1:
            districtIds.append(0)
        else:
            districtIds.append(districtId)
    return districtIds

def initRoutePaths(routeProblemInfo):
    paths = []
    for i in range(routeProblemInfo["numVehicles"]):
        nodeList = []
        path = createPathFromNodeList(routeProblemInfo, nodeList, True)
        paths.append(path)
    return paths

def printPathTimes(routeProblemInfo):
    print("Max Path Time : " + secondsToHoursStr(routeProblemInfo["maxPathTime"]))
    print("Min Path Time : " + secondsToHoursStr(routeProblemInfo["minPathTime"]))
    print("Total Path Time : " + secondsToHoursStr(routeProblemInfo["totalPathTime"]))

def getDistrictId(routeProblemInfo, nodeId):
    districtId = routeProblemInfo["districtInfoArr"][nodeId]["id"]
    if districtId == -1:
        return 0
    else:
        return districtId

def getNodeIndexByDistrictId(routeProblemInfo, districtId):
    for i in range(len(routeProblemInfo["districtInfoArr"])):
        tempDistrictId = routeProblemInfo["districtInfoArr"][i]["id"]
        if tempDistrictId == districtId:
            return i
    return -1

def getNomineeDistrictIds(routeProblemInfo):
    districtIds = []
    for nodeId in routeProblemInfo["nomineeNodes"]:
        districtId = getDistrictId(routeProblemInfo, nodeId)
        districtIds.append(districtId)
    return districtIds

def getUsedDistrictIds(routeProblemInfo):
    districtIds = []
    for nodeId in routeProblemInfo["usedNodes"]:
        districtId = getDistrictId(routeProblemInfo, nodeId)
        districtIds.append(districtId)
    return districtIds

def printAdditionResult(additionResult):
    print(additionResult["path"])
    print(secondsToHoursStr(additionResult["time"]))
    if additionResult["feasible"]:
        print("Feasible")
    else:
        print("Not feasible")

def createPathFromNodeList(routeProblemInfo, nodeList, deleteFromNomineeNodes):
    if deleteFromNomineeNodes:
        removeFromNomineeNodesList(routeProblemInfo, nodeList)
    path = {}
    pathNodeList = [0] + nodeList.copy() + [0]
    path["nodeList"] = pathNodeList
    pathTime = findPathDistance(routeProblemInfo, pathNodeList)
    path["time"]  = pathTime
    path["feasible"] = checkFeasibleTime(routeProblemInfo, pathTime)
    path["numDeliveries"] = findPathTotalDeliveries(routeProblemInfo, nodeList)
    return path

def findPathTotalDeliveries(routeProblemInfo, nodeList):
    numDeliveries = 0
    for nodeId in nodeList:
        numDeliveries += routeProblemInfo["districtInfoArr"][nodeId]["std"]
    return numDeliveries

def findTimeDiff(routeProblemInfo, path, nodeId, place):
    durationRemoved = findDurationByIndex(routeProblemInfo, path[place], path[place + 1])
    durationAdded1 = findDurationByIndex(routeProblemInfo, path[place], nodeId)
    durationAdded2 = findDurationByIndex(routeProblemInfo, nodeId, path[place + 1])
    return durationAdded1 + durationAdded2 - durationRemoved

def findOptimalPath(routeProblemInfo, nodeList, deleteFromNomineeNodes):
    allPerm = list(itertools.permutations(nodeList))
    bestPathTime = 99999999
    bestPath = createPathFromNodeList(routeProblemInfo, nodeList, deleteFromNomineeNodes)
    bestPathIsFeasible = False
    for perm in allPerm:
        tempPath = createPathFromNodeList(routeProblemInfo, list(perm), False)
        tempPathTime = tempPath["time"]
        if tempPathTime < bestPathTime:
            bestPath = tempPath.copy()
            bestPathTime = tempPathTime
            bestPathIsFeasible = bestPathTime <= routeProblemInfo["deliveryTimeLimit"]
    return bestPath, bestPathTime, bestPathIsFeasible

def optimizePath(routeProblemInfo, vehicleId):
    nodeList = routeProblemInfo["paths"][vehicleId]["nodeList"].copy()
    removeZeros(nodeList)
    if len(nodeList) <= 1:
        return
    bestPath, bestPathTime, bestPathIsFeasible = findOptimalPath(routeProblemInfo, nodeList, False)
    routeProblemInfo["paths"][vehicleId] = bestPath
    updateMaxMinPathTimes(routeProblemInfo)

def removeZeros(nodeList):
    while 0 in nodeList:
        nodeList.remove(0)

def checkFeasiblePathListOptimal(routeProblemInfo, pathList):
    for path in pathList:
        bestPath, bestPathDist, isFeasible = findOptimalPath(routeProblemInfo, path, False)
        if not isFeasible:
            return False
    return True

def checkFeasiblePath(routeProblemInfo, path):
    pathTime = findPathDistance(routeProblemInfo, path)
    return checkFeasibleTime(routeProblemInfo, pathTime)

def checkFeasiblePathOptimal(routeProblemInfo, path):
    bestPath, bestPathDist, isFeasible = findOptimalPath(routeProblemInfo, path, False)
    return isFeasible

def checkFeasibleTime(routeProblemInfo, pathTime):
    return pathTime <= routeProblemInfo["deliveryTimeLimit"]

def findPathDistance(routeProblemInfo, path):
    totDuration = 0
    nodeExists = nodeExistsInPath(path)
    if not nodeExists:
        return totDuration
    for i in range(len(path)-1):
        totDuration += findDurationByIndex(routeProblemInfo, path[i], path[i+1])
    for districtIndex in path:
        if districtIndex != 0:
            totDuration += calcVisitTimeForDistrict(routeProblemInfo, districtIndex)
    return totDuration

def nodeExistsInPath(path):
    for districtIndex in path:
        if districtIndex != 0:
            return True
    return False

def calcVisitTimeForDistrict(routeProblemInfo, districtIndex):
    stdVal = routeProblemInfo["districtInfoArr"][districtIndex]["std"]
    return int(stdVal * routeProblemInfo["avgDeliveryTime"])

def testScenarioWithoutSplit(routeProblemInfo):
    print("NOT READY YET")

def splitDistricts(districtInfoArr, districtDistances):
    districtInfoArrNew = []
    for districtInfo in districtInfoArr:
        districtId = districtInfo["id"]
        districtInfoNew = copy.deepcopy(districtInfo)
        districtInfoNew["id"] = districtId
        if districtId <= 0:
            districtInfoArrNew.append(districtInfo)
        else:
            stdVal = districtInfo["std"] / 2
            districtInfoNew["std"] = stdVal
            districtInfoNew["id"] = str(districtId) + "_1"
            districtInfoArrNew.append(districtInfoNew)
            districtInfoNew2 = copy.deepcopy(districtInfoNew)
            districtInfoNew2["id"] = str(districtId) + "_2"
            districtInfoArrNew.append(districtInfoNew2)
    distanceMatrixNew = []
    for i in range(len(districtInfoArrNew)):
        row = []
        for j in range(len(districtInfoArrNew)):
            id_i_str = districtInfoArrNew[i]["id"]
            id_i = getDistrictIdFromIdStr(id_i_str)
            id_j_str = districtInfoArrNew[j]["id"]
            id_j = getDistrictIdFromIdStr(id_j_str)
            row.append(findDistanceById(districtDistances, id_i, id_j))
        distanceMatrixNew.append(row)
    return districtInfoArrNew, distanceMatrixNew

def getDistrictIdFromIdStr(id_str):
    if id_str == -1:
        return -1
    else:
        words = id_str.split("_")
        return int(words[0])

def prepareDistictInfoArrAndDistanceMatrix(districtList, districtDistances, simDataXls):
    districtArr = [-1]
    for i in range(len(districtList)):
        districtItem = districtList[i]
        if districtItem["area"] != 0:
            districtArr.append(districtItem["id"])

    simDataForDistricts = simDataXls.parse("deliveries")
    dataForDistrictArr = []
    for i in range(len(simDataForDistricts)):
        dataForDistrict = {}
        dataForDistrict["id"] = simDataForDistricts.iloc[i, 0]
        dataForDistrict["std"]  = simDataForDistricts.iloc[i, 1]
        dataForDistrictArr.append(dataForDistrict)

    districtInfoArr = []
    infoForXdock = {}
    infoForXdock["id"] = -1
    districtInfoArr.append(infoForXdock)
    for districtId in districtArr:
        if districtId > 0:
            found = False
            for districtInfo in dataForDistrictArr:
                if districtId == districtInfo["id"]:
                    districtInfoArr.append(districtInfo)
                    found = True
            if not found:
                districtInfo = {}
                districtInfo["id"] = districtId
                districtInfo["std"] = 0
                districtInfoArr.append(districtInfo)
    distanceMatrix = []
    for i in districtArr:
        row = []
        for j in districtArr:
            row.append(findDistanceById(districtDistances, i, j))
        distanceMatrix.append(row)
    return districtInfoArr, distanceMatrix

def findDistanceById(districtDistances, fromId, toId):
    for i in range(len(districtDistances)):
        item = districtDistances[i]
        if item["fromId"] == fromId and  item["toId"] == toId:
            return [item["distance"], item["duration"]]
    return [-1, -1]

def findDistanceByIndex(routeProblemInfo, m, n):
    return routeProblemInfo["distanceMatrix"][m][n][0]

def findDurationByIndex(routeProblemInfo, m, n):
    return routeProblemInfo["distanceMatrix"][m][n][1]

def findDistanceByDistrictId(routeProblemInfo, m, n):
    nodeM = getNodeIndexByDistrictId(routeProblemInfo, m)
    nodeN = getNodeIndexByDistrictId(routeProblemInfo, n)
    return findDistanceByIndex(routeProblemInfo, nodeM, nodeN)

def findDurationByDistrictId(routeProblemInfo, m, n):
    nodeM = getNodeIndexByDistrictId(routeProblemInfo, m)
    nodeN = getNodeIndexByDistrictId(routeProblemInfo, n)
    return findDurationByIndex(routeProblemInfo, nodeM, nodeN)

def addToUsedNodes(routeProblemInfo, nodeId):
    routeProblemInfo["usedNodes"].append(nodeId)

def removeFromNomineeNodes(routeProblemInfo, nodeId):
    routeProblemInfo["nomineeNodes"].remove(nodeId)

def removeFromNomineeNodesList(routeProblemInfo, nodeList):
    for nodeId in nodeList:
        removeFromNomineeNodes(routeProblemInfo, nodeId)

def addAddNodeToRoute(routeProblemInfo):
    additionResult = checkAddNodeToRoute(routeProblemInfo)
    if additionResult["feasible"]:
        addNodeToPathNodeIdPlaceInner(routeProblemInfo, additionResult["vehicleId"], additionResult)
    return additionResult

def addNodeToRouteNodeId(routeProblemInfo, nodeId, opType):
    additionResult = checkAddNodeToRouteNodeId(routeProblemInfo, nodeId, opType)
    if additionResult["feasible"]:
        addNodeToPathNodeIdPlaceInner(routeProblemInfo, additionResult["vehicleId"], additionResult)
    return additionResult

def addNodeToPath(routeProblemInfo, vehicleId):
    additionResult = checkAddNodeToPath(routeProblemInfo, vehicleId)
    if additionResult["feasible"]:
        addNodeToPathNodeIdPlaceInner(routeProblemInfo, vehicleId, additionResult)
    return additionResult

def addNodeToPathNodeId(routeProblemInfo, vehicleId, nodeId):
    additionResult = checkAddNodeToPathNodeId(routeProblemInfo, vehicleId, nodeId)
    if additionResult["feasible"]:
        addNodeToPathNodeIdPlaceInner(routeProblemInfo, vehicleId, additionResult)
    return additionResult

def addNodeToPathNodeIdPlace(routeProblemInfo, vehicleId, nodeId, place):
    additionResult = checkAddNodeToPathNodeIdPlace(routeProblemInfo, vehicleId, nodeId, place)
    if additionResult["feasible"]:
        addNodeToPathNodeIdPlaceInner(routeProblemInfo, vehicleId, additionResult)
    return additionResult

def checkAddNodeToRoute(routeProblemInfo):
    paths = routeProblemInfo["paths"]
    bestTime = 99999999
    bestAdditionResult = None
    for vehicleId in range(routeProblemInfo["numVehicles"]):
        additionResult = checkAddNodeToPath(routeProblemInfo, vehicleId)
        if additionResult["feasible"]:
            maxPathTime = -1
            for i in range(len(paths)):
                path = paths[i]
                if i == vehicleId:
                    pathTime = additionResult["time"]
                else:
                    pathTime = path["time"]
                if pathTime > maxPathTime:
                    maxPathTime = pathTime
            if maxPathTime != -1 and maxPathTime < bestTime:
                bestTime = maxPathTime
                bestAdditionResult = copy.deepcopy(additionResult)
    return bestAdditionResult

def checkAddNodeToRouteNodeId(routeProblemInfo, nodeId, opType):
    if opType == "cheIns":
        paths = routeProblemInfo["paths"]
        bestTimeDiff = 99999999
        bestAdditionResult = {}
        bestAdditionResult["feasible"] = False
        for vehicleId in range(routeProblemInfo["numVehicles"]):
            additionResult = checkAddNodeToPathNodeId(routeProblemInfo, vehicleId, nodeId)
            if additionResult["feasible"]:
                timeDiff = additionResult["time"] - paths[vehicleId]["time"]
                if timeDiff < bestTimeDiff:
                    bestTimeDiff = timeDiff
                    bestAdditionResult = copy.deepcopy(additionResult)
        return bestAdditionResult
    if opType == "minPathTime":
        paths = routeProblemInfo["paths"]
        bestTime = 99999999
        bestAdditionResult = {}
        bestAdditionResult["feasible"] = False
        for vehicleId in range(routeProblemInfo["numVehicles"]):
            additionResult = checkAddNodeToPathNodeId(routeProblemInfo, vehicleId, nodeId)
            if additionResult["feasible"]:
                maxPathTime = -1
                for i in range(len(paths)):
                    path = paths[i]
                    if i == vehicleId:
                        pathTime = additionResult["time"]
                    else:
                        pathTime = path["time"]
                    if pathTime > maxPathTime:
                        maxPathTime = pathTime
                if maxPathTime != -1 and maxPathTime < bestTime:
                    bestTime = maxPathTime
                    bestAdditionResult = copy.deepcopy(additionResult)
        return bestAdditionResult

def checkAddNodeToPath(routeProblemInfo, vehicleId):
    additionResult = {}
    additionResult["vehicleId"] = vehicleId
    additionResult["feasible"] = False
    additionResult["node"] = -1
    additionResult["place"] = -1
    additionResult["time"] = 99999999
    for nomineeNode in routeProblemInfo["nomineeNodes"]:
        tempAdditionResult = checkAddNodeToPathNodeId(routeProblemInfo, vehicleId, nomineeNode)
        if tempAdditionResult["feasible"] and tempAdditionResult["time"] < additionResult["time"]:
            additionResult["feasible"] = tempAdditionResult["feasible"]
            additionResult["node"] = tempAdditionResult["node"]
            additionResult["place"] = tempAdditionResult["place"]
            additionResult["time"] = tempAdditionResult["time"]
    return additionResult

def checkAddNodeToPathNodeId(routeProblemInfo, vehicleId, nodeId):
    path = routeProblemInfo["paths"][vehicleId]["nodeList"]
    pathTime = routeProblemInfo["paths"][vehicleId]["time"]
    visitTimeForNode = calcVisitTimeForDistrict(routeProblemInfo, nodeId)
    additionResult = {}
    additionResult["vehicleId"] = vehicleId
    additionResult["feasible"] = False
    additionResult["node"] = nodeId
    additionResult["place"] = -1
    additionResult["time"] = 99999999
    for i in range(len(path) - 1):
        tempPlace = i
        timeDiff = findTimeDiff(routeProblemInfo, path, nodeId, i)
        tempPathTime = pathTime + timeDiff + visitTimeForNode
        tempPathIsFeasible = checkFeasibleTime(routeProblemInfo, tempPathTime)
        if tempPathIsFeasible and tempPathTime < additionResult["time"]:
            additionResult["place"] = tempPlace
            additionResult["time"] = tempPathTime
            additionResult["feasible"] = tempPathIsFeasible
    return additionResult

def checkAddNodeToPathNodeIdPlace(routeProblemInfo, vehicleId, nodeId, place):
    path = routeProblemInfo["paths"][vehicleId]["nodeList"]
    additionResult = {}
    additionResult["vehicleId"] = vehicleId
    additionResult["node"] = nodeId
    additionResult["place"] = place
    pathTime = findPathDistance(routeProblemInfo, path)
    visitTimeForNode = calcVisitTimeForDistrict(routeProblemInfo, nodeId)
    timeDiff = findTimeDiff(routeProblemInfo, path, nodeId, place)
    newPathTime = pathTime + visitTimeForNode + timeDiff
    additionResult["time"] = newPathTime
    additionResult["feasible"] = checkFeasibleTime(routeProblemInfo, newPathTime)
    return additionResult

def checkAddNodeToPathNodeIdPlaceWithPathTime(routeProblemInfo, vehicleId, nodeId, place, pathTime):
    path = routeProblemInfo["paths"][vehicleId]["nodeList"]
    additionResult = {}
    additionResult["vehicleId"] = vehicleId
    additionResult["node"] = nodeId
    additionResult["place"] = place
    visitTimeForNode = calcVisitTimeForDistrict(routeProblemInfo, nodeId)
    timeDiff = findTimeDiff(routeProblemInfo, path, nodeId, place)
    newPathTime = pathTime + visitTimeForNode + timeDiff
    additionResult["time"] = newPathTime
    additionResult["feasible"] = checkFeasibleTime(routeProblemInfo, newPathTime)
    return additionResult

def addNodeToPathNodeIdPlaceInner(routeProblemInfo, vehicleId, additionResult):
    removeFromNomineeNodes(routeProblemInfo, additionResult["node"])
    addToUsedNodes(routeProblemInfo, additionResult["node"])
    routeProblemInfo["paths"][vehicleId]["nodeList"].insert(additionResult["place"] + 1, additionResult["node"])
    routeProblemInfo["paths"][vehicleId]["time"] = additionResult["time"]
    routeProblemInfo["paths"][vehicleId]["numDeliveries"] += routeProblemInfo["districtInfoArr"][additionResult["node"]]["std"]
    updateMaxMinPathTimes(routeProblemInfo)
