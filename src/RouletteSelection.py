import random

def rouletteChoose(weights):
    totalWeight = 0
    for weight in weights:
        totalWeight += weight
    val = random.random()*totalWeight
    total = 0
    for i in range(len(weights)):
        total += weights[i]
        if val < total:
            return i
    return len(weights) - 1