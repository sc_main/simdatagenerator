from FileMethods import *
import xlsxwriter
import os
from DateMethods import *


def writeDeliverySimsExcelFile(deliverySims):
    nowStr = getNowStrMinimal()
    excelFileName = os.getcwd() + "\\output\\" + nowStr + "_deliverySims.xlsx"
    workbook = xlsxwriter.Workbook(excelFileName)
    worksheet = workbook.add_worksheet()

    worksheet.set_default_row(25)

    worksheet.set_column(0, 0, 12)
    worksheet.set_column(1, 4, 25)
    worksheet.set_column(5, 6, 15)
    worksheet.set_column(7, 10, 25)

    cell_format_header = workbook.add_format()
    cell_format_header.set_align('vcenter')
    cell_format_header.set_align('right')
    cell_format_header.set_bold()
    cell_format_header.set_font_color('#e6ffe6')
    cell_format_header.set_font_size(12)
    cell_format_header.set_border(1)
    cell_format_header.set_bg_color('#953205')

    cell_format_value = workbook.add_format()
    cell_format_value.set_align('vcenter')
    cell_format_value.set_align('right')
    cell_format_value.set_border(1)
    cell_format_value.set_font_size(12)
    cell_format_value.set_bg_color("#f2f2f2")

    row = 0
    col = 0

    worksheet.write(row, col, "Gonderi No", cell_format_header)
    worksheet.write(row, col + 1, "Olusturulma Tarihi", cell_format_header)
    worksheet.write(row, col + 2, "Gonderi Tipi", cell_format_header)
    worksheet.write(row, col + 3, "Slot", cell_format_header)
    worksheet.write(row, col + 4, "Mahalle", cell_format_header)
    worksheet.write(row, col + 5, "X-Koordinati", cell_format_header)
    worksheet.write(row, col + 6, "Y-Koordinati", cell_format_header)
    worksheet.write(row, col + 7, "Baslangic Saati", cell_format_header)
    worksheet.write(row, col + 8, "Bitis Saati", cell_format_header)
    worksheet.write(row, col + 9, "Hazirlanma Zamani", cell_format_header)
    worksheet.write(row, col + 10, "Urun Tipi", cell_format_header)

    row += 1

    for deliverySim in (deliverySims):
        worksheet.write(row, col, deliverySim["id"], cell_format_value)
        worksheet.write(row, col + 1, deliverySim["deliveryDate"], cell_format_value)
        worksheet.write(row, col + 2, deliverySim["deliveryTypeName"], cell_format_value)
        worksheet.write(row, col + 3, deliverySim["slotName"], cell_format_value)
        worksheet.write(row, col + 4, deliverySim["districtName"], cell_format_value)
        # deliveryDateStr = deliverySim["deliveryDate"]
        # dDate = strToDate(deliveryDateStr)
        # hour = dDate.hour
        # worksheet.write(row, col + 5, hour, cell_format_value)
        worksheet.write(row, col + 5, deliverySim["geoloc_lat"], cell_format_value)
        worksheet.write(row, col + 6, deliverySim["geoloc_lng"], cell_format_value)
        worksheet.write(row, col + 7, deliverySim["promisedBegDate"], cell_format_value)
        worksheet.write(row, col + 8, deliverySim["promisedEndDate"], cell_format_value)
        worksheet.write(row, col + 9, deliverySim["prepationDate"], cell_format_value)
        worksheet.write(row, col + 10, deliverySim["productTypeName"], cell_format_value)
        row += 1

    workbook.close()
    os.system("start excel.exe " + excelFileName)