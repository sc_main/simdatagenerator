

def normalizeArr(arr):
    tot = 0
    for i in range(len(arr)):
        tot += arr[i]
    if tot > 0:
        arr = [x / tot for x in arr]
    return arr
